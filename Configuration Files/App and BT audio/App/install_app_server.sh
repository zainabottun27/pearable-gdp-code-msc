#!/bin/bash +x
set -e
# Credits to iAmLevi https://blog.iamlevi.net/2017/05/control-raspberry-pi-android-bluetooth/
# This script has been created by the pearable gdp group 2017/18
# Tested with the "2018-04-17-raspbian-stretch-lite.img" image. Working however requires initial command line boot and slight edits as described in the guide
# The application for this is avaliable on the play store and this guide explaines how to run the Pi side host server

# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Welcome
echo
echo "Welcome to the Pearable Android Application Server Install"
echo "This enables our application to run on your Pi"
# Dependencies
echo
echo "Installing dependencies..."
# Apt-get update and upgrade to be run before this sh script, reboot needed 
apt-get --yes --force-yes install python-dev libbluetooth-dev
echo "Installing Python pybluez"
pip install pybluez
echo
echo "Installing Adafruit MCP3008 module - for temperature sensors"
pip install adafruit-mcp3008
echo "done."

# Start bluetooth daemon in compatibility mode
echo
echo "Editing Bluetooth daemon to start in compatibility mode..."
# This is adding a -C after the ExecStart command in the dbus-org.bluez.service
sed -i '/ExecStart/cExecStart=/usr/lib/bluetooth/bluetoothd -C' /lib/systemd/system/bluetooth.service
echo "done."


# Reloading the service configuration and restart the Bluetooth service
echo 
echo "Reloading system daemon"
systemctl daemon-reload
echo "done."

echo
echo "Restarting modified bluetooth service"
systemctl restart dbus-org.bluez.service
echo "done."
# Load serial port profile
echo
echo "Loading serial port profile"
sdptool add SP
echo "done."

# Copy app files from our git or clone if not already present
echo
cd /home/pi
if [ -d pearable-gdp-code ]; then
  echo "Updating pearablebtsrv files from our Bitbucket repo..."
  cd pearable-gdp-code/Configuration\ Files/App\ and\ BT\ audio/BluetoothAudio && git pull && git checkout ${1:master}
else
  echo "Downloading Bluetooth Audio from Bitbucket Repository..."
  git clone https://bitbucket.org/hearablegdp/pearable-gdp-code.git
  cd pearable-gdp-code/Configuration\ Files/App\ and\ BT\ audio/BluetoothAudio && git pull && git checkout ${1:master}
fi
echo "App Files updated/installed."
# Copy pearable folder from bitbucket to local home directory
echo
echo "Copying pearable folder from Bitbucket repo to local home directory"
cp -r /home/pi/pearable-gdp-code/pearable /home/pi/
echo "done."

# Copy services folder from bitbucket to local home directory
echo
echo "Copying services folder from Bitbucket repo to local home directory"
cp -r /home/pi/pearable-gdp-code/Service\ Files/ /home/pi/
echo "done."

# Make pearablebtsrv.py script executable
echo
echo "Making pearablebtsrv.py script executable"
chmod +x /home/pi/pearable/pearablebtsrv.py
echo "done."

# Make logSensors.py script executable
echo
echo "Making logSensors.py script executable"
chmod +x /home/pi/pearable/logSensors.py
echo "done."

# Register and start pearablebtsrv service with systemd
echo
echo "Registering and starting pearablebtsrv service with systemd..."
systemctl enable /home/pi/Service\ Files/pearablebtsrv.service
systemctl daemon-reload
if [ "`systemctl is-active pearablebtsrv`" != "active" ]; then
  systemctl start pearablebtsrv
else
  systemctl restart pearablebtsrv
fi
  systemctl status pearablebtsrv --full --no-pager
echo "done."

# Register and start sensorlog service with systemd
echo
echo "Registering and starting sensor logging service with systemd..."
systemctl enable /home/pi/Service\ Files/sensorlog.service
systemctl daemon-reload
if [ "`systemctl is-active sensorlog`" != "active" ]; then
  systemctl start sensorlog
else
  systemctl restart sensorlog
fi
  systemctl status sensorlog --full --no-pager
echo "done."


# Finished
echo
echo "All of our app dependencies and our pearable bluetooth server have now been installed successfully."
echo "This is running as pearablebtsrv.service which runs on boot and constantly runs in the background"
echo
echo "We have also installed a service called sensorlog.service this runs the sensor logging code."
echo "If you are not using our sensor board logs will be created that say not connected."
echo "If you connect our sensor board or another sensor using MCP ADC you should be able to modify our code to log your sensor values"
echo
echo "Please now install our app from the Play store (Android only) and you can use our project yourself"
echo "If you want to edit our app then the VisualStudio .sln files and all other scripts needed can be found in the link in our Bitbucket repo"
