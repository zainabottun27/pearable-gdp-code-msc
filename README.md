# README - GIT Guide and Repository Information 
:heart_eyes:
:green_heart:
:heartpulse:
:heart:
:ok_hand:

All of our software on the repo was written as part of our Masters group design project (GDP). All software has been written to confirm to the Apache Licence Version 2.0. The full details of this are provided in the root of this repo

The notice document details the notes regarding our licence, all software used and adapted was open-source and may be licenced accordingly by the original authors. Where necessary credit has been given to the original authors 

**_UPDATE 11/04/18: Our repository is now public. Anyone can read or clone our code however they will not be able to commit or push changes with out administrator approval. I have created an administrator account on this repo for a gdp email._**

**_Note: As the name of this repo has changed from gdp-code to pearable-gdp-code you will need to edit your config file to refrence the correct URL, to do this type `nano .git/config` from within your gdp-code directory, once editing this git config file update the project URL and now everything should work as normal._**

**_Instead of the above step you could also simply delete your local clone/folder, then re-clone the repo following the instructions._**

Here is our project repository. This contains commits (which are then uploaded, or pushed) from all team members and allows us to all pull, push and create branches of our code... As and when we write it.

Check out this website for more information on GIT Hub and Bitbucket. Basically it's just a really good way of maintaining, documenting and controlling versions when many people are working on the same code. We have a private repository on Bitbucket (for us to edit and make changes to). In addition to this we also have a public GitHub repository.

There are load of good guides and videos on the internet, I have had a go at writing a simple guide…  
**_Edited 11/04/18 to make guide clearer_**.  
For standard GIT usage the following commands will be all you need (after following the install instructions):  
- `git pull origin master` - pulls all files from BitBucket Repository  
- `git add .` - adds your changed files from machine  
- `git commit -m 'Your message'` - commits all files to repository - basically preps for pushing  
- `git push origin master` - pushes all changed/added files from your local commits to our BitBucket Repository with 'Your message'  


# Basic Git Tutorial
:computer:
## Initial Install Instructions - Only follow these once per machine.
1. **if on Windows install GITBash** - this is similar to command prompt but allows you to communicate with our git repository. On OSX (Mac) or Linux can just use the built in Terminal

2. **for the first use on your computer** run the following commands, this sets up your 		profile on git and links it to your BitBucket account. It means all your commits 	**will come in as you from your profile.**

	From within GITBash  
		**1.1. Set your username:**  
			`git config --global user.name "FIRST_NAME LAST_NAME"`  
		**1.2. Set your email:**  
			`git config --global user.email "MY_NAME@example.com"`  
  
	## Cloning (copying files) to local work station
	Now you are all set up on BitBucket and GitBash it’s time to clone our GDP 		repository (repo) to your local machine. For this you use the ‘clone’ command 		within git. It basically copies the repo as it is to your current directory (‘cd’)  

3. **Change your current directory** to be where you want the repo folders to go. This is done by the following command  
	`cd /full/path/to/where/you/want/repo`  
4. **run this command to clone**  
	`git clone addressToBitBucket`  
  
	Your address to BitBucket can be found at the top of this page. If on github this address will be different.

	For me it was an HTML address - https://jordyjwilliams@bitbucket.org/hearablegdp/pearable-gdp-code.git 
	you will most likely just have to change your username  
	*This function will clone the gdp-code repo into the your current directory. Before adding anything or pulling you will have to **cd into pearable-gdp-code***

## Daily GIT commands (Used when writing/editing code)
:computer:
Now you are basically all set up on git and BitBucket - well done.  
:thumbsup: :fire:
  
Here are the commands you will use to upload new files and edits (known as commit 	and push in git) and download (pull) to and from the repo.  
  
**Before adding any files make sure you are in repository `cd /path/to/cloned/repo`**  
  
1. **Adding files** - you can add any files from your local machine or any files you have made edits to.  

	Adding **all new and changed files**   
		`git add .`  
	Adding **specific file**  
		`git add FILENAME`
  
2. Now you have added the file you need to **‘commit’ the changes**. Each commit is ‘signed’ with your name/user (setup in step 2). Each commit needs to have a **message**.  

	Committing changes with message  
		`git commit -m ‘YOUR MESSAGE OF WHAT YOU WANT TO SAY. ABOUT COMMIT’`  
  
	*Note* if you do not put the `-m` after commit and just run `git commit` it will 	open Vi which is a really gross text editor. You can try to learn to 	use this if you want but it’s truly horrible.  
	:thumbsdown: :broken_heart: :crying_cat_face: :cry:  
	
3. **Pulling** (downloading most recent version)  
  
	**BEFORE ALL COMMITS please pull to your local folder**  
  
	You have to commit from a version that is **up to date with the repo**, cannot be 		behind, e.g. before you edit code or stuff **you should pull** (see Section 7).  
		`git pull origin`  

	*Note* You may need to change the `master` to another branch however this readme 	guide will not go into detail on this.  
  
4. **Pushing** (uploading commits)  
  
	Now you have added and committed all files you want, (you can commit more than 		once within a push) e.g. you could add two files then commit with a message, then 	add more and commit with another message for the different files. 

	To upload them to our repo you need to **push them** with this command.  
	`git push origin master`  
  
	*Note* You may need to change the `master` to another branch however this readme 	guide will not go into detail on this.  
	 **Before Pushing please read the pulling section!**  

## Extra Git Commands
1. Branches (not in detail)  
	You can fork the code to branches, however this is harder conceptually and has the 	potential to go wrong easier.  

	For more info on this google around and we can discuss as a team if there will ever be a time when branches are needed.  

2. **Checkout** (seeing if your branch is up to date with repository)
	In order to see if your local branch (on your computer) is up to date with the repository (eg before a pull request or push) then you can use the following command.  
	It's useful as it shows what files are not up to date etc.  
		`git checkout master`

*Thanks for reading this guide, I hope this helped and was useful for understanding GIT better.  
Love you all.  
Kindest regards  
Jordy :heart: :heart_eyes:*
