#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Sat May  5 13:33:32 2018

@author: scottvanderleeden
"""
import numpy as np


def Sum(x, width, feedThrough=False):
    # convert the 2d array into a 3d array of shape (width, buffer size,
    # number of bands)
    if feedThrough:
        return(x)
    data3d = x.T.reshape([width, x.shape[1],
                         int(x.shape[0]/width)], order='C')
    # sum data3d across the octave bands to give a 2d array of shape
    # (width, buffer size)
    y = np.sum(data3d, axis=2)
    return(y)
