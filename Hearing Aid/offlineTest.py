#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
@author: Scott van der Leeden
"""

# import scripts written for Pearable GDP:
import hearingAidVariables as HA
import freqShiftFunction as FSF
import noiseReduction as NR
import octaveBandFilt as OBF
import envelopeFunctionHilbert as EFH
import envelopeFunction as EF
import compressionFunction as CF
import gainFunction as GF
import octaveSum as OS

# import built-in modules
import numpy as np
from scipy.io import wavfile
import time
from matplotlib import pylab as plt


# Boolean feed through choice for frequency shift
fTFeedbackCont = False
# Boolean feed through choice for octave band filtering
fTBands = False
# Boolean feed through choice for envelope
fTEnvelope = False
# Boolean feed through choice for compression
fTCompression = False
# Boolean feed through choice for noise reduction
fTNoiseRed = True
# Boolean feed through choice for gain
fTGain = False
# Boolean feed through choice for octave band sum
fTOS = False


def offlineHA(bandIndex, wavName, oneBuffer=True, sine=False, seconds=None,
              blockLength=512, order=2, width=2, fShift=15, writeName=None,
              custom=None, envHilbert=False):
    """
    This script was written in order to plot the offline results of the
    functions to be run in real-time using VISR

    Performs the hearing aid processing on the wav file provided.

    If OneBufer is True then processing is done on one buffer only and results
    for the speed of the code is printed, telling you if it is fast enough.
    This is done by calculating the buffer latency fs/bufferSize, and
    determining whether the code will process in this time.
    If oneBuffer is false then the length of data is given by the length of the
    wavfile
    If a value for seconds is given, the computation will be done that many
    seconds on the wavfile
    If custom is given the hearing aid process will be performed on that signal

    Parameters
    ----------
    bandIndex: ndarray
        index of the bands being used out of [250, 500, 1k, 2k, 4k] Hz
    wavname: string
        name of the wavfile to read (no need to include '.wav' in the name)
    oneBuffer: Boolean
        determines whether to perform the test over one buffer only
         (default: True)
    impulse: Boolean
        determins whether to give the data input as an impulse instead of the
        wav file  (default: False)
    seconds: integar
        if given, the wav file will only run for this length of time
        (default: None)
    blocklength: integar
        the buffer that would be used for real time processing (default: 512)
    order: integar
        the order of the filters being used to octave band filter the data
        (default: 2)
    width: integar
        number of channels of the input signal/wav file (default: 2)
    writeName: string
        if given the wavfile will be saved using that name, if not given the
        wavfile will be saved as the file name with 'HearingAided' added to the
        end (no need to include '.wav' in the name)
    custom: ndarray
        if given the hearing aid process will be performed on this signal
    envHilbert: Boolean
        if True the envelope is found using the Hilbert transform, if False it
        is found using the better, but more computationally intensive function

    Returns
    -------
    filt: ndarray
        data after being filtered in to octave bands
    ev: ndarray
        the envelope of the input signal
    com: ndarray
        the compressed input signal
    output: ndarray
        the signal after going through all of the processes
    t: ndarray
        time vector for plotting
    data: ndarray
        the input data found from the wav file
    fs: integar
        sampling frequency used
    """
    # find the relavent hearing aid variables from the band index using rhe
    # function hearingAidVariables.py
    bands, G, LK, CR, UK, MG, tauA, tauR = HA.numBands(bandIndex)
    # print the bands being used
    print('Bands used: '+str(bands))
    # read the wavfile
    fs, data = wavfile.read(wavName+'.wav')
    # if the width is 1 make the data 2 channel by duplicating data in two
    # channels
    if width == 1:
        datanew = np.zeros([2, data.shape[0]])
        datanew[0, :] = data
        datanew[1, :] = data
        data = datanew
        width = 2
    # make the data the correct shape for the hearing aid functions
    else:
        data = data.reshape([data.shape[1], data.shape[0]])
    # create the correct time vector based on the sampling frequency
    t = np.arange(0, data.shape[1])/float(fs)
    # if statements to decide what to do
    # if oneBuffer is True only use one buffer of data
    if custom is not None:
        data = custom
    elif oneBuffer:
        # if impulse is true give an impulse as the input signal
        if sine:
            # # create an array of zeros
            # data = np.zeros_like(data[:, 0:blockLength])
            # # make the first sample a 1
            # data[0] = 1
            t = np.arange(0, blockLength)/float(fs)
            for i in range(width):
                data = np.zeros([width, blockLength])
                data[i] = np.sin(2*np.pi*10*t)
        # if impulse is False take one buffer of wavfile data as the input
        else:
            # redefine data to be only one blocklength long
            data = data[:, 0:blockLength]
    # if no seconds are provided leave the data the length of the whole wav
    # file
    elif seconds is None:
        pass
    # if seconds are given make the data the correct number of seconds long
    else:
        # seconds*fs gives the number of samples to use
        data = data[:, 0:seconds*fs]
    # the width at which the signal will be changed to when filtered into bands
    processWidth = width*len(bandIndex)
    # calculate the allowed time for the processing for realtime application
    allowedTime = float(blockLength)/fs
    # print the allowed time
    print('Allowed Time: '+str(allowedTime))
    # create a time stamp to use as the start time for the frequency shift
    tFreqShift = time.time()
    # use freqShiftFunction() to shift the data in the frequency domain by a
    # value of fShift
    dataFC = FSF.shift(data, data.shape[1], fs, fShift,
                       feedThrough=fTFeedbackCont)
    # find the time taken since the frequency shift time stamp
    freqShiftTime = time.time()-tFreqShift
    # print the time taken for the frequency shift part
    print("Frequency shift code needs %f seconds" % (freqShiftTime))
    tFiltStart = time.time()
    # use octaveBandFilt to filter the signal into octave bands
    # octaveBandFilt.getFilterCoeffs() gets the filter coefficients from the
    # sampling frequency, an array of band centre frequencies and the filter
    # order to use
    b, a = OBF.getFilterCoeffs(fs, bands, order)
    # octaveBandFilt.applyFilterCoeffs() applies the filter coefficients found
    # given the data to filter dataFC, b, a the number of channels
    # width, the buffer size and the array of band centre frequencies
    filt = OBF.applyFilterCoeffs(dataFC, b, a, width,
                                 bufferSize=data.shape[1],
                                 bands=bands, feedThrough=fTBands)
    # find the time taken since the filtering time stamp
    filtTime = time.time()-tFiltStart
    # print the time taken for the filtering part
    print("Filter code needs %f seconds" % (filtTime))
    # create a time stamp for the start of the envelope part
    tEvStart = time.time()
    # if envHilbert is True, use envelopeFunctionHilbert.py to find the
    # envelope
    if envHilbert:
        ev = EFH.envelope(filt)
    # if envHilbert is False, use envelopeFunction.py to find the
    # envelope this function is more desirable as it allows attack and release
    # time inputs, however it is a lot slower so doesn't work as well in real
    # time
    else:
        ev = EF.envelope(filt, fs, tauA, tauR, data.shape[1],
                         processWidth, feedThrough=fTEnvelope)

    # find the time taken since the envelope time stamp
    evTime = time.time()-tEvStart
    # print the envelope time
    print("Envelope code needs %f seconds" % (evTime))
    # create a time stamp for the start of the compression part
    tNrStart = time.time()
    # apply noise reduction using noiseReduction.py, this code works in
    # realtime by reducing the gain in buffers that do not have speech in,
    # this can't be tested propperly in this offlineTest script as the
    # processing is done on one buffer only or on the signal as a whole.
    nR = NR.red(filt, blockLength, ev, feedThrough=True)
    # find the time taken since the noise reduction time stamp
    nrTime = time.time()-tNrStart
    # print the length of time noise reduction took
    print("Noise reduction code needs %f seconds" % (nrTime))
    # create a time stamp for the start of the compression code
    tComStart = time.time()
    # use compressionFunction.py to compresss the signal given the
    # filtered signal, the envelope of the signal and the hearing aid
    # parameters found at the start
    com = CF.compress(nR, ev, LK, CR, UK, MG, width=processWidth,
                      feedThrough=fTCompression)
    # find the time taken since the compression time stamp
    comTime = time.time()-tComStart
    # print the compression time
    print("Compression code needs %f seconds" % (comTime))
    # create a time stamp for the start of the gain part
    tGainStart = time.time()
    # use gainFunction.py to compresss the signal given the compressed
    # signal and a vector of gains to be applied
    gain = GF.calculate(com, G, feedThrough=fTGain)
    # find the time taken since the gain time stamp
    gainTime = time.time()-tGainStart
    # print the compression time
    print("Gain code needs %f seconds" % (gainTime))
    # define the output as being the gain-applied signal
    output = OS.Sum(gain, width, feedThrough=fTOS)
    # find the time taken since the filtering time stamp (total time)
    totalTime = time.time()-tFiltStart
    # if statements to decide what to do based on only working for one buffer
    # or not:
    # if oneBuffer is true give information on the time it takes. Is the code
    # fast enough to run real time?
    if oneBuffer:
        # if the allowed time is greater than the total time then the code wont
        # underrun in real time
        if allowedTime > totalTime:
            # state that the code is fast enough and say how long was left in
            # the buffer
            print("Code is fast enough. Time remaining: %f seconds."
                  % (allowedTime-totalTime))
        # if the allowed time is less than the total time then the code will
        # underrun in real time
        else:
            # state that it is not fast enough and say how long by
            print("Whole code takes %f seconds too long" % (
                    totalTime-allowedTime))
    # if oneBuffer is false then write a wavfile from the data
    else:
        # make the output data the correct shape for a wavfile
        outWav = output.reshape([output.shape[1], output.shape[0]])
        # if no writeName is given, make one
        if writeName is None:
            # add HearingAided.wav to the end of the wavfile name
            writeName = wavName+str('HearingAided.wav')
        # if writeName is given add .wav to the end
        else:
            writeName = writeName+str('.wav')
        # writes the normalised output data to a wavfile
        wavfile.write(writeName, fs, outWav/np.max(outWav))
    # return the relavent variables
    return(bands, dataFC, filt, ev, nR, com, output, t, data, fs)


# choose a wavfile to test
testFilename = 'SpeechLeftRight'
# choose which bands to use by indexing them
# the bands are 250, 500, 1k, 2k and 4k Hz
bandIndex = np.array([0, 1, 2, 3, 4])
# here is a custom signal that can be chosen to use, it is a channel of zeros
# and a channel of ones, this can by applied to the hearing aid code to proove
# that the channels don't bleed into each other, the zeros channel should come
# out as zeros still
custom = np.array([np.zeros(512), np.ones(512)])
# Boolean to decide whether to process on one Buffer only
oneBuffer = False
# the number of seconds to process of the wavfile
seconds = None
# perform the offline test
bands, dataFC, filt, ev,\
    nR, com, output, t, data, fs = offlineHA(bandIndex,
                                             wavName=testFilename,
                                             oneBuffer=oneBuffer,
                                             writeName='SpeechLeftRight5',
                                             order=2,
                                             sine=False, seconds=seconds,
                                             width=2, custom=None)

"""
==================================
Basic plots of hearing aid process
==================================
"""
# if oneBuffer is True, don't plot because input and output data is no longer
# the same shape
if oneBuffer:
    pass
# if seconds is given, don't plot because input and output data is no longer
# the same shape
elif seconds is not None:
    pass
# else plot the data
else:
    # empty list for the plot legends
    leg = []
    # iterate through the channels to give each channel the appropriate label
    for i in range(len(bands)*2):
        if i >= len(bands):
            leg.append(str('Right: ')+str(bands[i-len(bands)]))
        else:
            leg.append(str('Left: ')+str(bands[i]))
    # decide which channel to plot
    channelIdx = 0
    # define a frequency vector
    f = np.linspace(0, fs, len(t))
    # transpose the data to make it suitable for plotting
    dataPlot = data.T
    dataFCPlot = dataFC.T
    filtPlot = filt.T
    evPlot = ev.T
    comPlot = com.T
    outputPlot = output.T

    # close all figures
    plt.close('all')

    # plot the frequency shifted data in the frequency domain with input
    plt.figure()
    plt.title('Frequency Shift Frequency Domain')
    plt.xlabel('Time (s)')
    plt.ylabel('Magnitude')
    plt.plot(f, np.abs(np.fft.fft(dataPlot[:, 0])), label='Input Signal')
    plt.plot(f, np.abs(np.fft.fft(dataFCPlot[:, 0])),
             label='Frequency Shifted Signal')
    plt.legend()
    plt.xlim(0, fs/2)

    # Plot the envelope with input
    plt.figure()
    plt.title('Envelope')
    plt.xlabel('Time (s)')
    plt.ylabel('Magnitude')
    plt.plot(t, filtPlot[:, channelIdx], label='Input')
    plt.plot(t, evPlot[:, channelIdx], label=leg[channelIdx])
    plt.legend(title='Octave Band (Hz)')

    # plot compression with input
    plt.figure()
    plt.title('Compression')
    plt.xlabel('Time (s)')
    plt.ylabel('Magnitude')
    plt.plot(t, filtPlot[:, channelIdx], label='Input')
    plt.plot(t, comPlot[:, channelIdx], label=leg[channelIdx])
    plt.legend(title='Octave Band (Hz)')

    # plot the hearing aid output with input
    plt.figure()
    plt.title('Hearing Aid Output')
    plt.xlabel('Time (s)')
    plt.ylabel('Magnitude')
    plt.plot(t, dataPlot[:, 0], label='Input')
    plt.plot(t, outputPlot[:, 0], label=leg[channelIdx], alpha=0.9)
    plt.legend(title='Octave Band (Hz)')
