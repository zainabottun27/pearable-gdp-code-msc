#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
@author: Scott van der Leeden
"""

import numpy as np


def envelope(x, fs, tauA, tauR, blockSize, processWidth, feedThrough=False):
    """
    Find the envelope of a signal
    Parameters
    ----------
    x: ndarray
        The signal that will be compressed
    fs: integar
        sampling frequency of the signal
    Hearing aid parameters:
    The following parameters are arrays of floats to be applied to each octave
    band in two channels (left and right ears), they have length up to 10 for 2
    channels and 5 octave bands
    tauA: ndarray
        attack times
    tauR: ndarry
        release times

    blockSize: integar
        the buffer size being used (this is the length of the signal x)
    processWidth: integar
        the width of the data x
    feedThrough: Boolean
        if True feed through audio only

    Returns
    -------
    env: ndarray
        the envelope of the signal

    see
    J.M. Kates, Digital Hearing Aids, Plural Publishing Inc., Abingdon,
    UK, 2008, pp. 220‐247
    for theory on this
    """
    # if feedthrough is True return the input as the output
    if feedThrough:
        return(x)
    # define an array of zeros of length processWidth
    env0 = np.zeros(processWidth)
    # define an array of zeros to append the envelope to
    env = np.zeros_like(x)
    # find the attack time constant
    alphaA = np.exp(-1/(tauA*fs))
    # find the release time constant
    alphaR = np.exp(-1/(tauR*fs))
    # set the index i to 1, this starts at 1 because access to the previous
    # index is needed (i-1)
    i = 1
    # initilising alpha array
    alpha = np.zeros(processWidth)
    # while loop iterates through each sample
    while (i <= blockSize-1):
        # calculate the absolute of x
        x_abs = np.abs(x[:, i])
        # define env0 as the previous envelope value
        env0 = env[:, i-1]
        # in each channel, find where the previous sample envelope is less than
        # or greater than the signal
        aIdx = np.where(x_abs > env0)
        rIdx = np.where(x_abs <= env0)
        # apply attack and release time constants to alpha array using the
        # previous indexes
        # the if != 0 is there because if aIdx or rIdx is an empty array an
        # error will occur
        if aIdx[0].shape[0] != 0:
            alpha[aIdx] = alphaA[aIdx]
        if rIdx[0].shape[0] != 0:
            alpha[rIdx] = alphaR[rIdx]
        # apply the attack and release time constants to the previous sample to
        # get the envelope value
        env[:, i] = alpha * env0 + (1 - alpha) * x_abs
        # increase index by 1 to loop back around
        i += 1
    # return the signal envelope
    return(env)
