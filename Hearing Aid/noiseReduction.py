# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# from scipy import weave
import numpy as np


def red(x, blockSize, env, feedThrough=False):
    """
    Reduces the noise in the signal by applying a gain between 0 and 1 whose
    value is:
            closer to 1 if the signal to noise ratio is high
            closer to 0 if the signal to noise ratio is low
    this means that the buffers with a poor signal to noise ratio are reduced
    in volume
    Parameters
    ----------
    x: ndarray
        the data to be shifted
    blockSize: integar
        the length of the data (best to be a power of 2)
    env: ndarray
        envelope of the input signal
    feedThrough: Boolean
        if True feed through audio only

    Returns
    -------
    y: ndarray
        noise reduced signal
    """
    if feedThrough:
        return(x)
    # array of zeros top assign the output y to
    y = np.zeros_like(x)
    # find the largest amplitude in the envelope for each channel
    largest = np.amax(env, axis=1)
    # find the smallest amplitude in the envelope for each channel
    smallest = np.min(env, axis=1)
    # estimate the signal to noise ratio
    SNR = 10*np.log10(largest/smallest)
    # define a gain quantity between 0 and 1 that is 1 for a perfect signal to
    # noise ratio
    W = SNR/(SNR+1)
    # apply the gain
    y = x*W[:, None]
    return(y)
