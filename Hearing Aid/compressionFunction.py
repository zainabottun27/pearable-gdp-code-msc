#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
@author: Scott van der Leeden
"""

import numpy as np
from scipy import interpolate


def compress(x, env, LK, CR, UK, MG, width, feedThrough=False):
    """
    Compresses a signal given the envelope and other compression parameters
    Parameters
    ----------
    x: ndarray
        The signal that will be compressed
    env: ndarray
        The envelope of x (must have same size as x)
    Hearing aid parameters:
    The following parameters are arrays of floats to be applied to each octave
    band in two channels (left and right ears), they have length up to 10 for 2
    channels and 5 octave bands
    LK: ndarray
        lower knees
    CR: ndarry
        compression ratios
    UK: ndarry
        upper knees
    MG: ndarray
        make-up gains
    feedThrough: Boolean
        if True feed through audio only

    Returns
    -------
    y: ndarray
        the compressed signal

    see
    J.M. Kates, Digital Hearing Aids, Plural Publishing Inc., Abingdon,
    UK, 2008, pp. 220‐247
    for theory on this
    """
    # if feedthrough is True return the input as the output
    if feedThrough:
        return(x)
    # array of zeros to assign the decibel valued envelope to
    envdB = np.zeros_like(env)
    # finds where the env will be less than 1
    nonzero = np.where(env > 1)
    # take the dB value of envelope where it is greater than one, this is to
    # prevent high negative decibels occuring
    envdB[nonzero] = 20*np.log10(env[nonzero])
    # an estimate for the maximum decibel value you would ever expect as the
    # input
    maxSig = 200
    # defines the x axis on the output/input plot for compression, defined
    # by the lower and upper knees, see:
    # J.M. Kates, Digital Hearing Aids, Plural Publishing Inc., Abingdon,
    # UK, 2008, pp. 220‐247
    # for theory on this
    In = np.array([np.zeros(width), LK, UK, np.linspace(maxSig, maxSig,
                   width)])
    # defines the y axis of the output/inut plot defined by the lower and
    # upper knees
    Out = np.array([np.zeros(width), LK, np.multiply((1./CR), (UK-LK))+LK,
                    np.multiply((1./CR), (UK-LK))+LK])
    # this next part of the code creates an interpolation class
    # for the output/input curve which allows you to give an envelope
    # as an input and recieve the desired output signal as an output
    # create an array of zeros to assign the desired output compression
    # signal to
    OutEnv = np.zeros_like(env)
    # iterate through each channel
    for i in range(width):
        # interpolate between the input and output axis defined above
        f = interpolate.interp1d(In[:, i], Out[:, i])
        # give the envelope as the input to the interpolation class to get
        # the desired output levels of the compressed signal
        OutEnv[i, :] = f(envdB[i, :])
    # by subtracting the output and input an array of dB gains to apply to the
    # signal to achieve the correct compression is found
    G = OutEnv-envdB
    # take the antilog of the gains
    g = np.power(10, (G/20))
    # take the antilog of the mark-up gain
    mg = np.power(10, (MG/20))[:, None]
    # apply the compression gain and the markup gain through multiplication
    y = np.multiply(g, x)*mg
    # output the compressed signal y
    return(y)
