# -*- coding: utf-8 -*-
"""
Created on Mon Apr 16 12:58:15 2018

@author: Christabel Goode

Storing heart rate data

"""
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Simple example of reading the MCP3008 analog input channels and printing
# them all out.
# Author: Tony DiCola
# License: Public Domain

import time
# Import SPI library (for hardware SPI) and MCP3008 library.
import Adafruit_MCP3008
import datetime
import numpy as np

current_dt = datetime.datetime.now()  # gets current date and time from computer
dt = str(current_dt).split(" ")  # splits date and time
date = dt[0].replace('-', '_')  # replaces '-' with '_' in date
timestamp = (dt[1].replace(':', '_'))[:5]  # replaces ':' with '_' in time
# print (date, time)

datafile = open("heart_rate_{}_{}.log".format(date, timestamp), 'w')

# Software SPI configuration:
# GPIO pins not hardware pin numbers
CLK  = 12  # 18 - we need to change this to try and not use SndCrd pins
MISO = 23
MOSI = 24
CS   = 25
mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)

# Main program loop.
while True:
    # Read all the ADC channel values in a list.
    values = [0]*8
    for i in range(8):
        # The read_adc function will get the value of the specified channel (0-7).
        values[i] = mcp.read_adc(i)
    # Print the ADC values.
    #print('{0:>4} {1:>4} {2:>4} {3:>4} {4:>4} {5:>4} {6:>4} {7:>4}'.format(*values))
    datafile.write('{1:>4},{2:>4},{3:>4},{4:>4},{5:>4},{6:>4},{7:>4}\n'.format(*values))
    # Could be changed to log the CPU time by adding a datetime.datetime.now().time() column
    # Pause for 5 ms. Total delay between samples is therefore approx 0.005*2.
    time.sleep(0.005)
