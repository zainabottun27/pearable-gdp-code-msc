# -*- coding: utf-8 -*-
"""
Created on Mon Mar 19 15:05:43 2018

@author: Christabel Goode

Plotting radial thermistor values

May be worth noting which thermistor we actually have:
NTCLE300E3103SB - THE 103 PART IS IMPORTANT WHEN LOOKING ON THE DATA SHEET
"""
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import matplotlib.pylab as plt
import scipy.interpolate as sc

temps = np.arange(-40, 130, 5)
#RT_R25 = np.array([33.21, 23.99, 17.52, 12.93, 9.636, 7.250, 5.505, 4.216, 3.255, 2.534, 1.987, 1.570, 1.249, 1.0, 0.8059, 0.6535, 0.5330, 0.4372, 0.3605, 0.2989, 0.249, 0.2084, 0.1753, 0.1481, 0.1256, 0.107, 0.09154, 0.07860, 0.06773, 0.05858, 0.05083, 0.04426, 0.03866, 0.03387])
RT = np.array([332.1, 239.9, 175.2, 129.3, 96.36, 72.5, 55.05, 42.16, 32.56, 25.34, 19.87, 15.7, 12.49, 10.0, 8.059, 6.535, 5.330, 4.372, 3.606, 2.989, 2.49, 2.084, 1.753, 1.481, 1.256, 1.070, 0.9154, 0.7860, 0.6773, 0.5858, 0.5083, 0.4426, 0.3866, 0.3387])*1000

#temp1 = sc.interp1d(RT_R25, temps, kind='quadratic')
KINDS = ['nearest', 'linear', 'zero', 'quadratic', 'cubic', 'slinear']
for i in range(len(KINDS)):
    temp = sc.interp1d(RT, temps, kind=KINDS[i])

    new_R = np.linspace(np.min(RT), np.max(RT), 100)

    ##plt.figure()
    ##plt.plot(RT_R25, temps, 'bo', label='Data point')
    ##plt.plot(RT_R25, temp1(RT_R25), 'g^--', label='Interpolated')
    ##plt.legend(loc='best')
    ##plt.xlabel(r'$R_T / R25 $')
    ##plt.ylabel(r'$Temperature,  \degree C$')

    plt.figure()
    plt.plot(RT/1000, temps, 'bo', label='Data point')
    plt.plot(new_R/1000, temp(new_R), 'g^--', label='Interpolated')
    plt.legend(loc='best')
    plt.xlabel(r'$Resistance\ (k\Omega)$')
    plt.ylabel(r'$Temperature\ (\degree C)$')
    title = 'Interpolation of temperature values using {} method'.format(KINDS[i])
    plt.title(title)
    
    plt.savefig('{}.png'.format(KINDS[i]))

plt.show()
