#!/usr/bin/python
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Trial code to log temp data.
import numpy as np
import readAnalogue
import Digital_Temp_Sensor as tempSens
import time
import datetime
import os.path
import os


# print (date, time)

#datafile = open("temperaturedata_{}_{}.log".format(date, timestamp), 'w')

def main():
    """ 
    This function gets the average number of temp values
    waitTime - seconds by def is 1
    """
    waitTime = 60  # 5s between each log reading
    while True:
        # This will now check the date each minuite when the log runs. Should solve ethernet issue.
        # Eg if pi then gets correct date new log will be created
        date = str(datetime.datetime.now()).split(" ")[0].replace("-", "_")
        filename = "sensordata_{}.log".format(date)
        path = "/home/pi/pearable/"
        completePath = os.path.join(path, filename)
        if os.path.exists(completePath):
            fileMode = 'a+'
        else:
            fileMode = 'w'
            # Add headers and datestamp the filename
        print(fileMode)
        datafile = open(completePath, fileMode)
        if fileMode == 'w':
            headers = ['Datestamp', 'Timestamp', 'Tmp36', 'Right Ear', 'Left Ear', 'Heart Rate - Left', 'Heart Rate - Right']
            datafile.write('\t'.join(headers)+'\n')
        current_dt = datetime.datetime.now()  # gets current date and time from computer
        dt = str(current_dt).split(" ")  # splits date and time
        date = dt[0].replace('-', '_')  # replaces '-' with '_' in date
        timestamp = (dt[1].replace(':', '_'))  # replaces ':' with '_' in time
        # If not present send back not connected 
        # Using try/except -zero division error will occur is no sensor
        try:
            tmp36 = readAnalogue.temp('tmp36', readAnalogue.channel_tmp36)
        except ZeroDivisionError:
            tmp36 = 'not connected'  # Setting if not connected
        try:
            tempR = readAnalogue.temp('radial', readAnalogue.channel_tempR)
        except ZeroDivisionError:
            tempR = 'not connected'  # Setting is not connected
        try:
            tempL = readAnalogue.temp('radial', readAnalogue.channel_tempL)
        except ZeroDivisionError:
            tempL = 'not connected'  # setting if not connected
        temps = [str(tmp36), str(tempR), str(tempL)]
        try:
            heartR = readAnalogue.readHeartRate(readAnalogue.channel_heartR, n=5)
        except ZeroDivisionError:
            heartR = 'not connected'
        try:
            heartL = readAnalogue.readHeartRate(readAnalogue.channel_heartL, n=5)
        except ZeroDivisionError:
            heartL = 'not connected'
        heartrates = [str(heartR), str(heartL)]
        # convert to strings in python for app.
        dataStr = "{0}\t{1}:\t".format(date, timestamp)+":"+"\t".join(temps)+"\t"+":"+"\t".join(heartrates)+"\n"
        datafile.write(dataStr)
        datafile.close()  # maybe change this to leave open
        time.sleep(waitTime)
# starting log
if __name__ == "__main__":
    main()
