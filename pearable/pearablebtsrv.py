#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Credit given to Levente Fuksz for raspibtsrv open-socurce script
# Imputs
import logging
import logging.handlers
import argparse
import sys
import os
import time
import threading
import subprocess
from bluetooth import *
import readAnalogue # Used for MCP chip to get analogue sensor data
# Hearing Test import - self written hearing test not currently workng
from hearingTest import runTest, pressed
import Digital_Temp_Sensor as tempSens
from readLog import readLog


class LoggerHelper(object):
    def __init__(self, logger, level):
        self.logger = logger
        self.level = level

    def write(self, message):
        if message.rstrip() != "":
            self.logger.log(self.level, message.rstrip())

    def flush(self):
        sys.stdout.flush()


def setup_logging():
    """ 
    This function allows the Pi to run the python script as a process recording all prints
    to the LOG_FILE location.
    """
    # Default logging settings
    LOG_FILE = "/var/log/pearablebtsrv.log"
    LOG_LEVEL = logging.INFO

    # Define and parse command line arguments
    argp = argparse.ArgumentParser(description="Raspberry Pi Bluetooth Server - For Pearable app")
    argp.add_argument("-l", "--log", help="log (default '" + LOG_FILE + "')")

    # Grab the log file from arguments
    args = argp.parse_args()
    if args.log:
        LOG_FILE = args.log

    # Setup the logger
    logger = logging.getLogger(__name__)
    # Set the log level
    logger.setLevel(LOG_LEVEL)
    # Make a rolling event log that resets at midnight and backs-up every 3days
    handler = logging.handlers.TimedRotatingFileHandler(LOG_FILE,
                                                        when="midnight",
                                                        backupCount=3)

    # Log messages should include time stamp and log level
    formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
    # Attach the formatter to the handler
    handler.setFormatter(formatter)
    # Attach the handler to the logger
    logger.addHandler(handler)

    # Replace stdout with logging to file at INFO level
    sys.stdout = LoggerHelper(logger, logging.INFO)
    # Replace stderr with logging to file at ERROR level
    sys.stderr = LoggerHelper(logger, logging.ERROR)


def threadWrapper(func, args, res=[]):
    """ 
    This function acts as a wrapper for threading processes
    """
    res.append(func(*args))


# Main loop
def main():
    """ 
    Main loop which calls logger function and enables BT socket communictiaon
    """
    # Setup logging
    setup_logging()

    # We need to wait until Bluetooth init is done
    time.sleep(10)

    # Make device visible
    os.system("hciconfig hci0 piscan")  # Bash command

    # Create a new server socket using RFCOMM protocol
    server_sock = BluetoothSocket(RFCOMM)
    # Bind to any port
    server_sock.bind(("", PORT_ANY))
    # Start listening
    server_sock.listen(1)

    # Get the port the server socket is listening
    port = server_sock.getsockname()[1]

    # The service UUID to advertise
    uuid = "7be1fcb3-5776-42fb-91fd-2ee7b5bbb86d"

    # Start advertising the service
    advertise_service(server_sock, "PearableBtSrv",
                      service_id=uuid,
                      service_classes=[uuid, SERIAL_PORT_CLASS],
                      profiles=[SERIAL_PORT_PROFILE])

    # These are the operations the service supports
    # Feel free to add more
    operations = ["Connect Headphones", "Connect Audio", "iCanHearIt",
                  "Freely", "Programmable", "Buttons", "Reboot", "Shut Down", "Test"]

    # Main Bluetooth server loop
    while True:

        print("Waiting for connection on RFCOMM channel %d" % port)

        try:
            client_sock = None

            # This will block until we get a new connection
            client_sock, client_info = server_sock.accept()
            # Printing the address connected.
            print("Accepted connection from ", client_info)

            # Read the data sent by the client
            data = client_sock.recv(1024)
            if len(data) == 0:
                break
            # Printing recieved data
            print("Received [%s]" % data)

            # Handle the request
            print(data)
            if data == "getop": # getop is sent by the phone app on connection
            # List of operations for 'overview page returned'
                response = "op:%s" % ",".join(operations)
            ### GETOP commands - sent for freely programmable page
            elif data == "Connect Headphones":
                #time.sleep(2)  # wait 2s
                subprocess.call(
                        "bash /home/pi/pearable/connectHeadphones.sh", shell=True)
                #time.sleep(2)
                response = "msg:Connecting headphones"
            elif data == "Start Hearing Test":
                response = "msg:Starting Hearing Test"
                runTest(btData=client_sock)
            elif data == "Reboot":
                subprocess.call("sudo reboot", shell=True)
                response = "msg:Rebooting Pi"
            elif data == "Shut Down":
                subprocess.call("sudo shutdown now", shell=True)
                response = "msg:Shutting down Pi"
            elif data == "iCanHearIt":
                response = "msg:Heard"
            elif data == "Freely" or data == "Programmable" or data == "Buttons":
                response = "msg:Edit Buttons here though Python Script on Pi"
            elif data == "Connect Audio":
                # Wait
                # time.sleep(2)
                # Setting Sink
                subprocess.call("bash /usr/bin/hacktooth/setSink.sh", shell=True)
                time.sleep(2)
                # Restarting Audio
                subprocess.call("sudo systemctl restart pulseaudio.service", shell=True)
                response = "msg:Reconnecting Audio"
                
            ### Commands from app - hardcoded buttons send these data strings
            elif data == "refreshSensors": # From sensor page on app
                # Data send back as strings so easier to interpret in C#
                # Data sent back in form - Temp, O2Sat, HeartRate
                # Digital Temp Sensor Try and except to catch if sensor not connected
                o2sat = "70"
                # Reading heart rate values from the log as these take ages to actually read
                # have to average over a long time
                try:
                    logVals = readLog(N=1)  # Reading last logged values
                    hLeft = (logVals[1][0])  # HR Left ear Conver to float
                    hRight = (logVals[2][0])
                    hr = [str(hRight), str(hLeft)]
                except IOError:  # Except the raised IOError from readLog if log not created
                    hr = ["Please wait","Please wait"]
                # Set HR to be actual MCP value
                try: 
                    # error thrown if the tempSens not working
                    # Can change the waitTime and ave number n
                    dTemp = tempSens.getTemp(n=1, waitTime=.1)
                except IOError:
                    digiTemp = False
                    dTemp = 'not connected'
                """ 
                if digiTemp: # if module/sensor present
                    imp.reload(tempSens)  # reloading to get recent value
                    t = tempSens.avg_temp # combine with other temp values
                    # Need data as strings so easier to convert and assign in c#
                """
                # If not present send back not connected 
                # Using try/except -zero division error will occur is no sensor
                try:
                    tmp36 = readAnalogue.temp('tmp36', readAnalogue.channel_tmp36)
                except ZeroDivisionError:
                    tmp36 = 'not connected'  # Setting if not connected
                try:
                    tempL = readAnalogue.temp('radial', readAnalogue.channel_tempL)
                except ZeroDivisionError:
                    tempL = 'not connected'  # Setting if not connected
                try:
                    tempR = readAnalogue.temp('radial', readAnalogue.channel_tempR)
                except ZeroDivisionError:
                    tempR = 'not connected'  # setting if not connected
                temps = [str(tmp36), str(tempR), str(tempL)]
                # convert to strings in python for app.
                response = "dataSensor:Refreshed Sensor Data:{}:{}:{}"\
                .format(",".join(temps),o2sat,",".join(hr)) 
            # Hearing aid Compression
            elif data.split(':')[0] == "DataComp":
                response = "msg:Appling Compression Changes"
            # Graph settings
            elif data.split(':')[0] == "DataGraphs":
                response = "msg:Appling Graph Changes"
            elif data.split(':')[0] == "Apply":
                if data.split(':')[1] == 'Layered':
                    value = data.split(':')[2]  # Value from slider
                    # Set layered listening value here...
                    response = "msg:Set Layered Value to {}".format(value)
                elif data.split(':')[1] == 'Gain':
                    values = data.split(':')[2]  # Values from sliders
                    # Set gain values here... look at format of how returned in log
                    response ="msg:Setting Gain"
                elif data.split(':')[1] == 'Compression':
                    values = data.split(':')[2]  # Values from sliders
                    # Set compression values here... look at format of how returned in log
                    response = "msg:Setting Compression" 
            elif data == "Analogue Sensors": # Old analogue sensor
                """ Commenting out analogue sensors - old function
                elegoo = readAnalogue.temp('elegoo_temp', readAnalogue.elegooCh)
                wire = readAnalogue.temp('wire_leaded', readAnalogue.wireCh)
                radial = readAnalogue.temp('radial', readAnalogue.radialCh)
                data = [elegoo, wire, radial]
                """
                data = 'misc'
                response = "msg:Analogue Data {}".format(data)
            elif data.split(':')[0] == "switch":
                type = data.split(':')[1]  # string of switch type
                value = data.split(':')[2]  # value of switch
                if type == 'ha':
                    # run code to set hearing aid here
                    type = 'Hearing Aid'
                    print('Turning hearing aid '+value)
                elif type == 'anc':
                    # run code to set ANC here
                    print('Turning ANC '+value)
                elif type == 'layered': # Layered listening
                    print('Turning layered '+value)
                elif type == 'cloud':
                    print('Turning Cloud Backup '+value)
                response = 'msg:Setting {} {}'.format(type, value)
            elif data == "refreshGraphs":
                sensorVals = str(readLog(N=10))
                response = 'msg:Graphs Refreshed {}'.format(sensorVals)
            # Insert more here
            else: # For other data items - prevending crash
                response = "msg:Not supported"

            client_sock.send(response)
            print("Sent back [%s]" % response)

        except IOError:
            pass

        except KeyboardInterrupt:

            if client_sock is not None:
                client_sock.close()

            server_sock.close()

            print("Server going down")
            break


main()
