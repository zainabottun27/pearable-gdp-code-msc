# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Mon Nov 13 12:05:26 2017

@author: Jordan Williams
"""
import numpy as np
from duplexAudio import duplexAudio
import time
# NOTE: Possibly need to change to use aplay not duplexAudio
# TODO: Get test playing over different channels.
# Stardard fs
rate = 44100
# Signal length in seconds (can be changed in functions)
signalLength = 2
# For duplexAudio
blockLen = 1024
signalHeard = False


def sine(freq, amplitude, length=signalLength, fs=rate):
    """
    This function generates a sine wave of a given frequency, amplutude and
    duration.
    Inputs:

    freq - float or int - frequency of returned data
    amplitude of data - float or int
    length - length of signal in seconds
    fs - sampling frequency

    Returns:
    data - np array of the data
    """
    deltaT = 1/float(fs)
    t = np.arange(0, length, deltaT)
    data = amplitude*np.sin(2*np.pi*freq*t)
    return data


def generateTones(freq, amplitudes=[0.01, 0.1, 0.3]):
    """
    This function generates an array of arrays (with the number of elements
    that are in amplitudes). Each element is a sine wave of specified freq
    with differing amplitudes.

    For loop with list comprehension is used.
    """
    tones = [sine(freq=freq, amplitude=amplitudes[i])
             for i in range(len(amplitudes))]
    return tones


def pressed(state):
    """
    This function takes state and changes the global variable to state.
    State is boolean. Needed to be as a function as it is called by a button
    press from within an app.
    """
    # Define signalHeard as global
    global signalHeard
    if state:
        signalHeard = True
        return signalHeard
    else:
        signalHeard = False
        return signalHeard


def runTest(testFreqs=[1000, 3000], amplitudes=[0.01, 0.1, 0.3], waitTime=1,
            btData=None):
    """
    This function runs a very basic hearing test when called.
    Relises on the state of global variable signalHeard
    waitTime is the time between each sound in s
    """
    results = np.zeros([len(testFreqs), len(amplitudes)])
    for f in range(len(testFreqs)):
        tones = generateTones(freq=testFreqs[f], amplitudes=amplitudes)
        for t in range(len(tones)):
            duplexAudio(tones[t], rate, blockLen)
            time.sleep(waitTime)
            if btData is not None:
                newData = btData.recv(1024)
                if newData == 'iCanHearIt':
                    results[f, t] = True
            if (signalHeard):
                # Button Pressed (heard tone)
                results[f, t] = True
                print("heard")
                pressed(False)  # Resetting to False
                time.sleep(1)  # waiting 1s for after
            else:
                # Button not pressed
                results[f, t] = False
                print("not heard")
    # Processing results
    if np.all(results):
        # All tests passed
        resText = 'All tests passed'
    else:
        resText = 'Tests Failed: \n'
        for f in range(len(testFreqs)):
            failedIdx = np.where(results[f] == False)[0]
            # Allowing for all freqs passed or only one failed
            if failedIdx.any() or len(failedIdx) == 1:
                resText += '{} Hz at:\n'.format(testFreqs[f])
            for i in range(len(failedIdx)):
                resText += '{} amplitude\n'.format(
                        amplitudes[i])
        resText += 'All other tests passed'
    print(resText)
    return results, resText
