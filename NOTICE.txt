In addition to the Apache License, Version 2.0 used for the original works of this project detailed in LICENCE.txt supporting packages have been written and used under their own open-source licences.

Every effort has been made to give credit where due to original author(s). If you are an author and feel this has not been done please contact us.

The main works that have been adapted for this project have all been used under a fair-use policy, and are governed by their own respective software licences.

The main authors that we would like to extend credit to are:
• Tony DiCola - for the sensor reading code and modules that use the MCP3008 ADC
• Matt Flax - FlatMax AudioInjector install files
• Levente Fuksz - iAmLevi - for the raspibtsrv and raspibtcontrol application which were largely adapted to form our solution
• LukasJapan - for the bluetoothspeaker code which was adapted to form our bluetoothaudio service
• Andreas Franck - for VISR

Work in the "Prototype and Developmental" folder Work in this folder is not used in the final release of any of our install files and is such only governed by existing licences. The unreleased work is strictly for development only and has not been licensed accordingly. 

If two licences are given or stated in a code file then the original works written have been combined by members of the GDP group