import numpy as np

def propagation_delay():
	""""""
	# Acoustic paramaters
	rho = 1.2	# density of air
	C = 340		# speed of sound
	j = 1j
	
	# Duct parameters
	r = 0.19/2			# radius of duct
	A = np.pi*r**2		# area of duct
	
	# Signal parameters
	f = 300				# frequency
	lam = C/f			# wavelength of disturbance
	k = (np.pi*2)/lam	# wavenumber
	x = 1.8 			# position in the duct
	w = 2*np.pi*f 		# omega
	t = C/x				# time
	Q0 = 0.1

	# Pressure at x
	Zc = (rho*C)/(2*A)	# characteristic equation
	q = Q0*np.exp(j*w*t)
	P_x = Zc*q*np.exp(-j*k*x)

	return P_x

P_x = propagation_delay()
print(P_x)


def propagation_delay2():
	""""""
	
