# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import time
import alsaaudio
from scipy.io import wavfile
import numpy as np
from numpy import reshape
import matplotlib.pyplot as plt
import wave
import padasip as pad
import scipy.signal as signal
from copy import deepcopy
from Feedback_control import feedbk_cnt
from FxLMS import fxlms



# audio variables
channels = 2                        # number of channels
fs = 32000							# sample rate in Hz
period_size = 1024                   # The number of frames in between each hardware interrupt.
itera = 625

# set input device parameters
inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, 'hw:0,0')
inp.setchannels(2)
inp.setrate(fs)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
inp.setperiodsize(period_size)


# set output device parameters
out = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, alsaaudio.PCM_NORMAL, 'hw:0,0')
out.setchannels(2)
out.setrate(fs)
out.setformat(alsaaudio.PCM_FORMAT_S16_LE)
out.setperiodsize(period_size)



def main():

	e_n_out = np.zeros((itera, period_size))		# empty matrix for error data

	count = 0
	
	while count < itera - 1:
		l, data = inp.read()	# read data and length of buffer
		if l == period_size:    # check that the data collection is equal to the period_size
			num = np.fromstring(data, dtype='int16')    # convert to numerical data

			# split left and right mic channels
			l = deepcopy(num)
			left = l[0::2]				

			r = deepcopy(num)
			right = r[1::2]

			# calculate error signal from LS output - should just be the mic signal
			e_n = left
			e_n_out[count, :] = e_n
			print(count)
			count += 1


	# adaptive filter seems to be leading which is weird "/

	# flatten from matrix
	e_n_est = e_n_out.ravel()
	# save error data as a txt file
	np.savetxt('Error output no ANC.txt', e_n_est)

	# plot of output
	plt.figure()	
	plt.plot(e_n_est[0:6000])
	plt.ylabel('Amplitude')
	plt.title('Reference signal, error signal and adaptive filter output')
	plt.legend()
	plt.show()

if __name__ == '__main__':
        main()
