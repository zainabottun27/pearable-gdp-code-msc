# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import alsaaudio
import sys
import time
from scipy.io import wavfile
import numpy as np
from numpy import reshape
import matplotlib.pyplot as plt
import wave
import padasip as pad
import scipy.signal as signal
from copy import deepcopy


# audio variables
channels = 2                        # number of channels
sample_size = 2                     # number of analog samples in bytes
frame_size = channels*sample_size   # one sample being played irrespective of channels or bits
									# for stereo and S16_LE: 2*(16 bits) = 4 bytes per sample
sample_rate = 44100					# sample rate in Hz
bps_rate = sample_size*sample_rate  # bytes per second data required to sustain the system. =channels*sample_size*analog_rate
									# = frame_size*analog_rate
bps_rate = sample_rate*frame_size   # the number of bytes that are processed per unit of time.
period_size = 256                   # The number of frames in between each hardware interrupt.
									# Currently this will create an interrupt every 0.02s

###### set input device parameters
inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, 'hw:0,0')
inp.setchannels(2)
inp.setrate(sample_rate)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
inp.setperiodsize(period_size)

###### set output device parameters
out = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, alsaaudio.PCM_NORMAL, 'hw:0,0')
out.setchannels(2)
out.setrate(sample_rate)
out.setformat(alsaaudio.PCM_FORMAT_S16_LE)
out.setperiodsize(period_size)

##### Filter parameters #####
n = 100	 # filter length (number of taps)
mu = 0.000001  # step size
itera = 100	 # number of iterations

###### To LMS #####
def main():
	
	inpu = np.zeros((itera, period_size))
	errorm = np.zeros((itera, period_size))

	count = 0	# counter

	while count < itera - 1:

		l, data = inp.read()	# read data and length of buffer

		if l == period_size:    # check that the data collection is equal to the period_size
			num = np.fromstring(data, dtype='int16')    # convert to numerical data

			# split left and right input channels
			l = deepcopy(num)
			left = l[0::2]				
			errorm[count, :] = left

			r = deepcopy(num)
			right = r[1::2]
			inpu[count, :] = right


			count += 1


	
	# flatten from matrix
	graph = inpu.ravel()
	er = errorm.ravel()
	
	# find the cross correlation
	zeros = np.zeros(len(graph))
	zeroR = np.concatenate((graph, zeros))
	zeroE = np.concatenate((er, zeros))
	FFTR = np.fft.rfft(zeroR)
	FFTE = np.fft.rfft(zeroE)
	conjE = np.conj(FFTE)
	cross = np.fft.irfft(FFTR*conjE)
	print(cross)
	
	cross2 = signal.
	er, graph)
	#In an autocorrelation, which is the cross-correlation of a signal 
	#with itself, there will always be a peak at a lag of zero, and its size will be the signal energy.

	# plot of output	
	plt.plot(graph, label='ref')
	plt.plot(er, label='error')
	plt.legend()
	plt.ylabel('Amplitude')
	plt.title('Reference signal and error signal amplitude')

	plt.figure()
	plt.plot(np.abs(cross), label='cross correlation')
	#plt.plot(cross2[44100/2:44100], label='cross correlation 2')
	plt.legend()
	plt.xlabel('Samples')
	plt.show()


if __name__ == '__main__':
	main()

