# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import alsaaudio
import sys
import time
from scipy.io import wavfile
import numpy as np
from numpy import reshape
import matplotlib.pyplot as plt
import wave
import padasip as pad
import scipy.signal as signal
from copy import deepcopy
from Feedback_control import feedbk_cnt
from Feedback_control_notch import feedbk_cnt_nt
from FxLMS import fxlms



# audio variables
channels = 2                        # number of channels
fs = 32000							# sample rate in Hz
period_size = 1024                   # The number of frames in between each hardware interrupt.

# set input device parameters
inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, 'hw:0,0')
inp.setchannels(2)
inp.setrate(fs)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
inp.setperiodsize(period_size)


# set output device parameters
out = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, alsaaudio.PCM_NORMAL, 'hw:0,0')
out.setchannels(2)
out.setrate(fs)
out.setformat(alsaaudio.PCM_FORMAT_S16_LE)
out.setperiodsize(period_size)


# set filter parameters
n = 2	 			# filter length (number of taps)
mu = 0.000000000001  	# step size
itera = 625	 		# number of iterations

###### To LMS #####
def main():

	# empty vectors to store data
	u_n_out = np.zeros((itera, period_size))			# empty matrix for ref_est data
	ref_out = np.zeros((itera, period_size))		# empty matrix for input data
	e_n_out = np.zeros((itera, period_size))		# empty matrix for error data

	fbk_out = np.zeros((itera, period_size))
	coeffs = np.zeros((itera, n))					# empty vector for coefficients
	
	###### To LMS #####
	ref_est = np.zeros(period_size)					# starting empty array for ref estimate
	coeffs[1, :] = signal.firwin(n, cutoff=0.3, window='hamming')		# initial coefficients
	count = 0					# itertive counter
	
		#### for writing into buffers
	buffer1left = np.zeros(period_size)
	buffer1right = np.zeros(period_size)

	buffer2left = np.zeros(period_size)
	buffer2right = np.zeros(period_size)
	
	# buffer to store feedback control data
	fed_back_buf = np.zeros(period_size)

	# buffer for x_hat
	x_hat_buf = np.zeros(period_size)
	
	while count < itera - 1:


		# obtain first buffer of input data and store
		if count == 0:
			l, data = inp.read()				# read mic data and length of buffer
			if l == period_size:    			# check that the data collection is equal to the period_size

				# convert mic data to numerical data
				num = np.fromstring(data, dtype='int16')

				# split left and right mic channels
				l = deepcopy(num)
				left = l[0::2]				
				buffer1left = left		# store error data

				r = deepcopy(num)
				right = r[1::2]
				buffer1right = right		# store ref data

				count += 1

		else:

			# obtain x'
			l, data = inp.read()	# read data and length of buffer
			if l == period_size:    # check that the data collection is equal to the period_size
				num = np.fromstring(data, dtype='int16')    # convert to numerical data

			# split left and right mic channels
				l = deepcopy(num)
				left = l[0::2]				
				buffer2left = left		# assign error data to buffer 2

				r = deepcopy(num)
				right = r[1::2]
			#	buffer2right = right		# assign ref data to buffer 2
				ref_out[count, :] = right	# store noise data

				# subtract buffer to get feedback control
				
				# aplpy feedback
			#	x_hat = feedbk_cnt_nt(right, period_size)
			#	fbk_out[count, :] = x_hat
				
				
			#	print(fed_back_buf[0:5])
			#	fbk_out[count, :] = fed_back_buf
				
				x_hat = right - fed_back_buf		# need initial fed_back buffer, so buffer it completes the second buffer, then afterwards it can call this data
				x_hat_buf = x_hat		# store in buffer?? 								# alternatively could implement ffedback control not on the first cycle, - might be easier
				x_hat2 = deepcopy(x_hat)	# to plant response
				
				# create time history of the input signals from the joined buffers
				overlap = n - 2			# calculate length of buffer needed to overlap
				joined = np.concatenate((buffer1right[(-1*overlap)-1:], x_hat_buf))	# join buffers together to process
				time_hist = pad.input_from_history(joined, n, bias=False)
				x_hat_th = time_hist[:, ::-1]
				print(x_hat_th.shape)

				# filter all samples in the buffer with the FIR filter
				transpose = np.transpose(coeffs[count, :])							# take the transpose of the time history of input
				u_n = np.dot(x_hat_th, transpose)		# filter the time history of the input to get un
				print(u_n.shape)
				u_n_out[count, :] = u_n 				# write out u_n data
				

				# call feedback control and send it to the buffer
				fed_back_buf = feedbk_cnt(u_n, period_size)	

				# filter x_hat_2 with the plant response
				r_hat = fxlms(x_hat2, period_size)

				# calculate error signal from LS output - should just be the mic signal
				e_n = left
				e_n_out[count, :] = e_n
				
				# calculate power in input signal
				power = np.sum(r_hat**2) #/len(r_hat)
				# update the NLMS block - x needs to change to a different filter
				coeffs[count+1, :] = coeffs[count, :] + (mu*e_n[-1]*r_hat[-(n+1):-1])/power	# update coefficient
				

				# add 0 back into array
				ls = deepcopy(u_n)
				ls = ls.reshape((len(ls)/2, 2))
				ls = np.insert(ls, (0, 1), 0, axis=1)
				ls = ls.flatten()
			

	
				#buffer1left = left 		# overwrite old data to store this data (might have to go at then end)
				buffer1right = x_hat	# overwrite old data to make room for new data coming in

				# write out data to loudspeaker
				data_proc = np.array(ls, dtype=np.int16)		# convert back to alsa data
				out.write(data_proc)
				print(count)
				count += 1


	# adaptive filter seems to be leading which is weird "/

	# flatten from matrix
	reference = ref_out.ravel()
	u_n_est = u_n_out.ravel()
	e_n_est = e_n_out.ravel()
	feed = fbk_out.ravel()
	
	# save NLMS data as a txt file
	np.savetxt('NLMS output plant.txt', u_n_est)
	
	# save reference data as a txt file
	np.savetxt('Reference noise plant.txt', reference)
	
	# save error data as a txt file
	np.savetxt('Error output ANC.txt', e_n_est)
	

	# plot of output	
	plt.subplot(211)
	plt.plot(reference[0:6000], label='reference')
	plt.plot(u_n_est[0:6000], label='adaptive filter')
	plt.plot(feed[0:6000], label='feedback control')
	plt.ylabel('Amplitude')
	plt.title('Reference signal, error signal and adaptive filter output')
	plt.legend()
	plt.subplot(212)
	plt.plot(e_n_est[0:6000], label='error')
	plt.legend()
	plt.ylabel('Amplitude')
	plt.xlabel('Samples')
	plt.show()
	
	### FFT
	##trans = np.fft.fft(filtered)
	##freqs = np.arange(0, sample_rate, (sample_rate*1.0/len(trans)))

	### plot of FFT of output with feedback	
	##plt.figure()
	##plt.plot(freqs, np.abs(trans))
	##plt.xlim(0, 20000)
	##plt.title('FFT of the reference signal')
	##plt.xlabel('frequency (Hz)')
	##plt.ylabel('FFT Magnitude')
	##plt.show()
	
	### Plot of the transfer function coefficients
	#plt.figure()
	#plt.plot(signal.firwin(n, cutoff=0.3, window="hamming"), label='Original')
	#plt.plot(coeffs[1, :], label='First')
	#plt.plot(coeffs[2, :], label='Second')
	#plt.plot(coeffs[-3, :], label='Third last')
	#plt.plot(coeffs[-2, :], label='Second last')
	#plt.plot(coeffs[-1, :], label='Last')
	#plt.title('Transfer function of plant')
	#plt.xlabel('Coefficient number')
	#plt.ylabel('Value')
	#plt.legend()
	#plt.show()
	


if __name__ == '__main__':
        main()



