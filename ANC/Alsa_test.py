# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import alsaaudio as audio
import time
from scipy.io import wavfile
# import visr
# visr::audiointerfaces::AudioInterface

periodsize = 2**10
audioformat = audio.PCM_FORMAT_S16_LE
channels = 1
framerate = 44100
card_info = {}

for device_number, card_name in enumerate(audio.cards()):
    card_info[card_name] = "hw:%s,0" % device_number
print(card_info)


inp = audio.PCM(audio.PCM_CAPTURE, audio.PCM_NONBLOCK)
inp.setchannels(channels)
inp.setrate(framerate)
inp.setformat(audioformat)
inp.setperiodsize(periodsize)

out = audio.PCM(audio.PCM_PLAYBACK)
out.setchannels(channels)
out.setrate(framerate)
out.setformat(audioformat)
out.setperiodsize(periodsize)

#buffer = new_fvec(framesize, channels)

# read the input
allData = bytearray()
count = 0
while True:
    # reading the input one long bytearray
    l, data = inp.read()
    for b in data:
        allData.append(b)
    
    # Just an ending condition
    count += 1
    if count == 44100*5:
        break
    
    # time.sleep(.001)

# splitting the bytearray into period sized chunks
list1 = [allData[i:i+periodsize] for i in range(0, len(allData), periodsize)]

# writing the output
for arr in list1:
    out.write(arr)



