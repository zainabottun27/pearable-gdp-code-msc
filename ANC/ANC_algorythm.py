# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 19:35:49 2017

@author: Scott
"""
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np


def ANC_filter(x, e, L1, L2, fs):
    """
    x = input
    e = error signal
    L1 = distance between reference sensor and secondary source
    L2 = distance betweem secondary source and error sensor
    fs = sampling frequency
    """
    c = 343
    # speed of sound
    T1 = L1 / c
    # time delay due to distance L1
    T2 = L2 / c
    # time delay due to distance L2
    delay1 = T1 * fs
    # delay in samples due to T1
    delay2 = T2 * fs
    # delay in samples due to T2
    X = np.fft.fft(x)
    # fourier transforms x to get X(k)
    n = np.arange(0, np.size(x), 1)
    # vector of samples
    epsilon = np.zeros([np.size(n)])
    # empty vector to index instantaneous squared error epsilon to
    w = np.zeros([np.size(n+1)])
    # empty vector to index adaptive filter TF W to
    t = np.arange(0, np.size(x)/fs, 1./fs)
    # time vector
    f = np.linspace(0, fs, len(X))
    # frequency vector
    omega = 2 * np.pi * f
    # angular frequency vector
    Z = np.exp(-1j * omega * t)
    # Z transform
    P = (1 / L1)*Z**(-delay1)
    # acoustic TF due to primary path represented in samples
    S = (1 / L2)*Z**(-delay2)
    # acoustic TF due to primary path represented in samples
    s = np.fft.fft(S)
    # inverse fourier of secondary path TF S
    X_dash = np.multiply(S, X)
    # input after secondary path effects have been applied (filterred
    # reference signal)
    x_dash = np.fft.fft(X_dash)
    # inverse fourier X_dash
    P_xdash = np.mean(np.square(x_dash))
    # power of the filtered reference signal
    mu = 1. / P_xdash[n + delay2]
    # maximum step size obtainable for LMS alrgorythm
    for i in n:
        epsilon[n] = e[n]**2
        w[n + 1] = w(n) - mu * np.gradient(epsilon[n]) / 2
    # this for loop implements the itterative LMS algorythm to reach a value
    # for the adaptive filter W
    w = w[0:n[np.size(w - 1)]]
    # takes of a sample from w to make it a suitable length
    W = np.fft.fft(w)
    # fourier transforms w
    Y_dash = S * W * X
    # calculates the secondary source (cancelling loud speaker) output
    E = P * X - Y_dash
    # calculates the error between the digital and physical systems
    return(E)
    # outputs the error
