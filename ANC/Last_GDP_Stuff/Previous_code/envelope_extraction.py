from scipy import weave
from numpy import exp

def time_constants(fs,tauA,tauR):
	alphaA = exp(-1/(tauA*fs))
	alphaR = exp(-1/(tauR*fs))
	return alphaA,alphaR

def calculate(x,alphaA,alphaR,N,env0,env):
	c_code = """
	
	int i = 0;
	double alpha;
	double x_abs;
	double y0;
	
	while (i <= N) {
	
		x_abs = fabs(x[i]);
		
		if (i == 0) {
			y0 = env0;
		}
		else {
			y0 = env[i-1];
		}
		
		if ( x_abs > y0 ) {
			alpha = alphaA;
		}
		else {
			alpha = alphaR;
		}
		
		env[i] = alpha * y0 + (1 - alpha) * x_abs;
		
		i++;
	}
	
	env0 = env[N-1];
	
	return_val = env0;
	
	"""
	env0 = weave.inline(c_code,['N','x','alphaA','alphaR','env0','env'],compiler='gcc')
	return env,env0

# Uses the smooth branch peak detector
# tauA is the attack time set to 0.005 in the config file. tauR is the release
# time set to 0.02 s in the config file. The function time_constant calcualtes
# the outputs alphaA and alphaR. These arise from modelling the smoothing
# effect of the attack and the release times as a digital one pole filter.
# The attack time is the time taken for the envelope to reach a certain
# increase and the release time is the time taken for the envelope to reach a
# certain decrease. 

# 1) Calculate the attack and release times using the smooth branch peak
#    detector equation
# 2) While the sample index is less than the chunk length
# 3) Calculate the absolute value of the raw input data at that sample. y0 is
#    is the envelope variable.
# 4) If the sample is equal to zero, the envelope is env0
# 5) Otherwise set y0 equal to the envelop of the previous sample
# 6) If the absolute value of x is greater than y0 (greater than the envelope),
#    set the variable alpha to the attack time
# 7) Otherwise set the variable alpha to the release time
# 8) The envelope of each sample is then the time constant * envelope of the
#    previous sample plus (1-time constant) * absolute value of x
# 9) Count up to get the next sample and continue the loop
# 10) Set the initial envelope env0 to the envelope of the last sample in the
#     chunk.
# 11) Return the envelope data set and the initial envelope for the chunk env0