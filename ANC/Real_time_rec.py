# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

##arecord -f dat -d 4 -D hw:0,0 t.wav # record audio
##aplay -f dat t.wav  # play recording

import alsaaudio
import sys
import time
from scipy.io import wavfile
import numpy as np

channels = 1                        # number of channels
sample_size = 1                     # number of samples
frame_size = channels*sample_size
# when you buffer N samples, you create a frame of data. You can then output sequential frames of data at a rate which is 1/N times
# the sample rate of the sampled signal.

frame_rate = 44100
byte_rate = frame_rate*frame_size   # the number of bytes that are processed per unit of time.
period_size = 160                   # the period size controls the internal number of frames per second. It is a multiple of frame_size

# PCM stands for Pulse Code Modulation. It is the standard form of digital audio in computers. IN a PCM stream the amplitude of the
# analogue signal is sampled at reqular intervals and each sample is quantized to the nearest value within a range of digital steps.

######

# set output to playback audio. PCM_normal mode blocks until a full period is available and then returns a tuple (length,data)
# where length is the number of frames of captured data, and data is the capture sound frames as a string.
# length of data = periodsize*framesize in bytes
out = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, alsaaudio.PCM_NORMAL)
out.setchannels(channels)                   # set the number of channels
out.setformat(alsaaudio.PCM_FORMAT_S16_LE)  # set the data format
out.setrate(frame_rate)                     # set the frame rate
out.setperiodsize(period_size)              # set the period size

######

# set input to record audio
inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL)
inp.setchannels(channels)
inp.setrate(frame_rate)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
inp.setperiodsize(period_size)

######

def main():
        
        buf = []
        count = 0
        while True:
            l, data = inp.read()  # length of data is 160, the same as one period as expected.
            print(data)
            buf.append(data)
            count += 1
            if count == 400:
                break
          #  wavfile.write('t.wav', 44100, np.array(buf))
##            if len(buf)>=10:
##                out.write(buf[0])
##                wavfile.write(buf[0], 't.wav')
##                del buf[0]

        return buf
       
if __name__ == '__main__':
        main()