# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import scipy.signal as signal

thresh = 5
taps = 1024
bnd = 0.03
fft_size = 1024




def feedbk_cnt_nt(u_n, period_size):
	


	signal_fft = np.fft.rfft(u_n)
	signal_fft_magnitude = np.absolute(signal_fft)
	max_index = np.argmax(signal_fft_magnitude)
	max_value = signal_fft_magnitude[max_index]

	if max_value > np.mean(signal_fft_magnitude) + thresh*np.std(signal_fft_magnitude):
		cf = max_index*(1.0/(signal_fft_magnitude.size))
		fs=12000

		h_notch = signal.firwin(taps-1, [max(0.01,cf-bnd/(fs)), min(cf+bnd/(fs),0.99)])
		h_notchfft = np.fft.rfft(h_notch, fft_size)
		signal_fft = signal_fft*h_notchfft
	
	signal_IFFT = np.fft.irfft(signal_fft)

	return signal_IFFT

#if __name__ == '__main__':
        #main()
