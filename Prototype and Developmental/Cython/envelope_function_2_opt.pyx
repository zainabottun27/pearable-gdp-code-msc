import numpy as np
import pylab as py

cpdef double calculate(double x,int fs,int tauA,int tauR,int N, double env,
                      int width,double env0):
    cdef:
        double x_abs, aIdx, rIdx
        int alphaA, alphaR, i
    alphaA = np.exp(-1./(tauA*fs))
    alphaR = np.exp(-1./(tauR*fs))
    i = 0
    x_abs = np.abs(x)
    alpha = np.zeros(width)
    y0 = np.zeros(width)
    while (i <= N-1):
#        x_abs = np.abs(x[:][i])
        if (i == 0):
            y0 = env0
        else:
            y0 = env[:][i-1]
 
        aIdx = np.where(x_abs[:][i] > y0)
        rIdx = np.where(x_abs[:][i] <= y0)
        alpha[aIdx] = alphaA
        alpha[rIdx] = alphaR
#        for n in range(0, x_abs.shape[0]):
#        for n in range(0, x_abs[:][i].shape[0]):
#            if x_abs[n][i]>y0[n]:
#                alpha[n] = alphaA
#            else:
#                alpha[n] = alphaR
        env[:][i] = alpha * y0 + (1. - alpha) * x_abs;
        env0 = env[:][N-1];
        i+=1

    return(env)
