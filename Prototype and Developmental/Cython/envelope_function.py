import numpy as np
import pylab as py


def calculate(x,fs,tauA,tauR,N,env,numCh,env0=np.zeros(2)):
    alphaA = np.exp(-1/(tauA*fs))
    alphaR = np.exp(-1/(tauR*fs))
    i = 0
    # initilising alpha array
    alpha = np.zeros(numCh)
    while (i <= N-1):
        x_abs = np.abs(x[:,i])
        if (i == 0):
            y0 = env0
        else:
            y0 = env[:,i-1]
        # Using np.where instead of if to set both channels
        aIdx = np.where(x_abs > y0)
        rIdx = np.where(x_abs <= y0)
        """
        Old method using if statement for each channel
        if ( x_abs[0] > y0[0] ):
            alpha[0] = alphaA
        else:
            alpha[0] = alphaR
        # right channel
        if ( x_abs[1] > y0[1] ):
            alpha[1] = alphaA
        else:
            alpha[1] = alphaR
        """
        alpha[aIdx] = alphaA
        alpha[rIdx] = alphaR
        env[:,i] = alpha * y0 + (1 - alpha) * x_abs;
        env0 = env[:,N-1];
        i+=1
    """
    # plot attempts
    py.figure()
    py.plot(x, label='signal')
    py.plot(env, lebel='envelope')
    py.legend(loc=0)
    """
#    return(env)
    return(x)
