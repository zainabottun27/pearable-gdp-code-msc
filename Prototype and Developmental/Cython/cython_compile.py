#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 10:49:16 2018

@author: pi
"""

from distutils.core import setup
from Cython.Build import cythonize

setup(ext_modules = cythonize('envelope_function_2_opt.pyx'))