#!/bin/bash
# This script takes a target bt mac address
# Target MAC address as sink
targetMAC='00_18_09_9E_A8_21'

# Getting list of sinks to varable

sinksList=$(pactl list sinks short)

# For each line/item in list

for i in $sinksList
do
if [[ $i == *$targetMAC* ]]; then
  	echo "Bluetooth Headphones connected"
  	desiredSink=$i
fi
done

# If no BT connected set to soundcard
if [[ -z "$desiredSink" ]]; then
	echo "No Bluetooth Headphones connected"
	# Might need to change this desired sink line
	desiredSink=alsa_output.platform-soc_sound.analog-stereo
fi
# Setting desiredSink in /etc/btpulsemix.conf
echo "Audio sink set to: $desiredSink"
sudo sed -i "/AUDIOSINK/c\AUDIOSINK=$desiredSink" /etc/btpulsemix.conf


