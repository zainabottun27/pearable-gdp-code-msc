#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  9 12:30:45 2018

@author: pi
"""

import numpy as np
from scipy import signal


def getFilterCoeffs(fs, bands, order):
    """
    Creates filter coefficients for each octave band filter.
    Creates a chain of band pass filters.
    The lowest octave band is a low pass filter only.
    The highest octave band is a high pass filter only.

    Parameters
    ----------
    fs: integar
        sampling frequency
    bands: ndarray
        centre frequencies of the octave bands being used
    order: integar
        order of the filters to be used for the band pass filters
        for the high and low pass filters this order is doubled to get the
        same number of filter coefficients for all types of filters

    Returns
    -------
    b: 2D array shape (number of bands, 2*order+1)
        b filter coefficients
    a: 2D array shape (number of bands, 2*order+1)
        a filter coefficients
    """
    fD = 2**0.5
    fU, fL = bands*fD, bands/fD  # gets the lower and upper octave band freqs
    Wn = np.array([fL, fU])/(0.5*fs)
    b = np.zeros([len(bands), order*2+1])
    a = np.zeros([len(bands), order*2+1])
    if len(bands) == 1:
        b, a = signal.butter(order, Wn, btype='band', output='ba')
    else:
        for i in range((len(bands))):
            if i == 0:
                b[i, :], a[i, :] = signal.butter(order*2, Wn[:, i][1],
                                                 btype='low', output='ba')
            elif i == (len(bands)-1):
                b[i, :], a[i, :] = signal.butter(order*2, Wn[:, i][0],
                                                 btype='high', output='ba')
            else:
                b[i, :], a[i, :] = signal.butter(order, Wn[:, i],
                                                 btype='band', output='ba')
    return (b, a)


def applyFilterCoeffs(x, b, a, width, bufferSize, bands):
    """
    Creates filter coefficients for each octave band filter.
    Creates a chain of band pass filters.
    The lowest octave band is a low pass filter only.
    The highest octave band is a high pass filter only.

    Parameters
    ----------
    x: ndarray
        signal to be filtered
    b: ndarray
        b coefficients
    a: ndarray
        a coefficients
    width: integer
        number of channels x has
    bufferSize: integer
        length of the signal x (size of the processing buffer)
    bands: ndarry
        centre frequencies of the octave bands

    Returns
    -------
    y: ndarray shape (width*number of bands, buffer size)
        filtered signal
    """
    # create an empty 3D array to assign y to of shape (x channels, buffer
    # size, number of bands)
    y = np.zeros([width, bufferSize, len(bands)])
    # if only one band is chosen
    if len(bands) == 1:
        # apply the coefficients as normal
        y = signal.lfilter(b, a, x)
    # if multiple bands are chosen
    else:
        # apply the coefficients to each band in order using a for loop
        for i in range(len(bands)):
            y[:, :, i] = signal.lfilter(b[i, :], a[i, :], x)
    # reshape the 3D array y to be 2D of shape (x channels* number of bands)
    y = y.T.reshape([y.shape[0]*len(bands), y.shape[1]], order='A')
    return(y)