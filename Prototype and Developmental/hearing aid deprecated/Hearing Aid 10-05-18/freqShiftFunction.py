#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Wed May  2 10:58:05 2018

@author: pi
"""

import numpy as np


def shift(x, blockSize, fs, fShift, feedThrough=False):
    """
    Performs a shift in the frequency domain and converts back to the time
    domain
    Parameters
    ----------
    x: ndarray
        the data to be shifted
    blockSize: integar
        the length of the data (best to be a power of 2)
    fs: integar
        sampling frequency of the data
    fShift: integar
        the frequency to shift the up by, this will be approximate as the exact
        frequncy shift is limited to sampling limitaitons
    Returns
    -------
    xShift: ndarray
        the time domain, frequency shifted data
    """
    # if feedThrough is True return the input as the output
    if feedThrough:
        return(x)
    # compute the fourier transfrom of the data
    X = np.fft.rfft(x, n=blockSize)
    # work out the number of samples to shift the data by based on the desired
    # frequency shift
    sampleShift = fShift*blockSize/(fs/2)
    # if the number of samples is less than one then shift by one sample
    # instead
    if sampleShift < 1:
        sampleShift = 1
    # make sampleShift an integar for indexing (this will round down to the
    # nearest whole number)
    sampleShift = np.int(sampleShift)
    # create an array of zeros for the shifted signal
    XShiftF = np.zeros_like(X)
    # use indexing to shift the signal, the frequencies below fShift will be 0
    XShiftF[:, sampleShift:] = X[:, :(X.shape[1]-sampleShift)]
    # inverse Fourier transform the signal to get it back into the time domain
    xShift = np.fft.irfft(XShiftF, n=blockSize)
    # return the frequency shifted signal
    return(xShift)
