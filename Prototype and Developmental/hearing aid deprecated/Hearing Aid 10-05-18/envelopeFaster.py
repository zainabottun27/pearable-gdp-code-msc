#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Fri May  4 21:22:16 2018

@author: scottvanderleeden
"""

import numpy as np


def envelope(x, fs, tauA, tauR, blockSize, processWidth, feedThrough=False):
    if feedThrough:
        return(x)
    env0 = np.zeros(processWidth)
    env = np.zeros_like(x)
    alphaA = np.exp(-1/(tauA*fs))
    alphaR = np.exp(-1/(tauR*fs))
    i = 1
    alpha = np.zeros(processWidth)
    x_abs = np.abs(x)
    while (i <= blockSize-1):
        env0 = env[:, i-1]
        # Using np.where instead of if to set both channels
        aIdx = np.where(x_abs[:, i] > env0)
        rIdx = np.where(x_abs[:, i] <= env0)

        if aIdx[0].shape[0] != 0:
            alpha[aIdx] = alphaA[aIdx]
        if rIdx[0].shape[0] != 0:
            alpha[rIdx] = alphaR[rIdx]
        env[:, i] = alpha * env0 + (1 - alpha) * x_abs[:, i]
        i += 1
    return(env)
