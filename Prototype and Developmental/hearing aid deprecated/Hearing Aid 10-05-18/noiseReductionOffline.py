#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Fri Mar 23 15:16:29 2018

@author: pi
This script was written in order to plot the offline results of the functions
to be run in real-time using VISR
"""
import envelopeFunction
import envelopeOffline
import compressionFunction
import gainFunction
import octaveBandFilt
import numpy as np
from scipy.io import wavfile
import time
import hearingAidVariables as HA
import freqShiftFunction as FS
import noiseReduction as NR
import octaveSum as OS
from matplotlib import pylab as plt

# Offline tests of compression and envelope algorithms


def offlineHA(bands, wavName, oneBuffer=True, impulse=False, seconds=None,
              blockLength=20, order=2, width=2, fShift=15, writeName=None):
    """
    Performs the hearing aid processing on the wav file provided.
    If OneBufer is True then processing is done on one buffer only and results
    for the speed of the code is printed, telling you if it is fast enough
    If oneBuffer is false then the length of data is given by the length of the
    wavfile
    If a value for seconds is given, the computation will be done that many
    seconds on the wavfile
    Parameters
    ----------
    bandIndex: ndarry
        index of the bands being used out of [250, 500, 1k, 2k, 4k] Hz
    wavname: string
        name of the wavfile to read (no need to include '.wav' in the name)
    oneBuffer: Boolean
        determines whether to perform the test over one buffer only
         (default: True)
    impulse: Boolean
        determins whether to give the data input as an impulse instead of the
        wav file  (default: False)
    seconds: integar
        if given, the wav file will only run for this length of time
        (default: None)
    blocklength: integar
        the buffer that would be used for real time processing (default: 512)
    order: integar
        the order of the filters being used to octave band filter the data
        (default: 2)
    width: integar
        number of channels of the input signal/wav file (default: 2)
    writeName: string
        if given the wavfile will be saved using that name, if not given the
        wavfile will be saved as the file name with 'HearingAided' added to the
        end (no need to include '.wav' in the name)

    Returns
    -------
    filt: ndarry
        data after being filtered in to octave bands
    ev: ndarry
        the envelope of the input signal
    com: ndarry
        the compressed input signal
    output: ndarry
        the signal after going through all of the processes
    t: ndarray
        time vector for plotting
    data: ndarry
        the input data found from the wav file
    fs: integar
        sampling frequency used
    """
    # the width at which the signal will be changed to when filtered into bands
    processWidth = width*len(bands)
    # print the bands being used
    print('Bands used: '+str(bands))
    # read the wavfile
    fs, data = wavfile.read(wavName+'.wav')
    # make the data the correct shape for the hearing aid functions
    data = data.reshape([width, data.shape[0]])
    # use octaveBandFilt to filter the signal into octave bands
    # octaveBandFilt.getFilterCoeffs() gets the filter coefficients from the
    # sampling frequency, an array of band centre frequencies and the filter
    # order to use
    b, a = octaveBandFilt.getFilterCoeffs(fs, bands, order)
    # octaveBandFilt.applyFilterCoeffs() applies the filter coefficients found
    # given the data to filter dataFS, b, a the number of channels
    # width, the buffer size and the array of band centre frequencies
    filt = octaveBandFilt.applyFilterCoeffs(data, b, a, width,
                                            bufferSize=data.shape[1],
                                            bands=bands)
    ev = envelopeOffline.envelope(filt, fs, 0.0002, 0.0004, data.shape[1],
                                  processWidth)
    noiseRed = NR.red(filt, blockSize=data.shape[1], env=ev)
    output = OS.Sum(noiseRed, width)
    # if statements to decide what to do
    # if oneBuffer is True only use one buffer of data
    if oneBuffer:
        # if impulse is true give an impulse as the input signal
        if impulse:
            # create an array of zeros
            data = np.zeros_like(data[:, 0:blockLength])
            # make the first sample a 1
            data[0] = 1
        # if impulse is False take one buffer of wavfile data as the input
        else:
            # redefine data to be only one blocklength long
            data = data[:, 0:blockLength]
    # if no seconds are provided leave the data the length of the whole wav
    # file
    elif seconds is None:
        pass
    # if seconds are given make the data the correct number of seconds long
    else:
        # seconds*fs gives the number of samples to use
        data = data[:, 0:seconds*fs]
    # create the correct time vector based on the sampling frequency
    t = np.arange(0, data.shape[1])/float(fs)
    # make the output data the correct shape for a wavfile
    outWav = output.reshape([output.shape[1], width])
    # if no writeName is given, make one
    if writeName is None:
        # add HearingAided.wav to the end of the wavfile name
        writeName = wavName+str('HearingAided.wav')
    # if writeName is given add .wav to the end
    else:
        writeName = writeName+str('.wav')
    # writes the normalised output data to a wavfile
    wavfile.write(writeName, fs, outWav/np.max(outWav))
    # return the relavent variables
    return(noiseRed, t, data, fs)


# choose a wavfile to test. 'snarky.wav' is a 2ch 48000 fs file
testFilename = 'noiseySpeech'
# choose which bands to use byu indexing them
bands = np.array([250, 500, 100])
# perform the offline test
noiseRed, t, data, fs = offlineHA(bands, wavName=testFilename, oneBuffer=False,
                                  order=2, width=1,
                                  writeName=testFilename+'noiseRed')



# plotting filter
#FILT1 = np.fft.fft(filt)
#DATA = np.fft.fft(data)
#f = np.linspace(0, fs, len(t))
#py.close('all')
#py.figure()
#py.semilogx(f[:len(t)/2], np.transpose(DATA[:, :len(t)/2]))
##py.semilogx(f[:len(t)/2], np.transpose(FILT1[:,:len(t)/2]))
#py.ylim(-2, 2)
#py.xlim(0, 8000)
# Plotting - envelope
#py.figure()
#py.plot(t, data[0],color='C0', label='wav file - L')
##py.plot(t, data[1],color='C0', ls='--', label='wav file - R')
#py.plot(t, ev[0],color='C1', label='Envelope - L')
##py.plot(t, ev[1],color='C1',ls='--', label='Envelope - R')
#py.legend(loc=0)
#py.xlabel(r'$Time - [s]$')
#py.ylabel(r'$Amplitude - [a.u.]$')
#py.title('Offline processing of Signal Evelope of {} File'.format(testFilename))
#py.savefig('EnvelopePlot')
#py.show()

## Plotting - compression
#py.figure()
#py.plot(t, data[0],color='C0', ls='-',label='wav file - L')
#py.plot(t, data[1],color='C0', ls='--',label='wav file - R')
#py.plot(t, com[0],color='C1', ls='-',label='Compressed - L')
#py.plot(t, com[1],color='C1', ls='--',label='Compressed - R')
#py.legend(loc=0)
#py.xlabel(r'$Time - [s]$')
#py.ylabel(r'$Amplitude - [a.u.]$')
#py.title('Offline processing of Compression of {} File'.format(testFilename))
#py.savefig('CompressionPlot')
#py.show()