#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Mon Mar 19 19:06:15 2018

@author: pi
"""


def calculate(x, G_dB, feedThrough=False):
    """
    Provides a simple gain to the signal provided
    Parameters
    ----------
    x: ndarry
        signal
    G_dB: ndarry
        array of decibel gains for each channel
    """
    # if feedThrough is True return the input as the output
    if feedThrough:
        return(x)
    # antilog the dB gain provided [:, None] performs a transpose
    G = 10**(G_dB[:, None]/20.0)
    # multiply the signal by that
    y = G*x
    return(y)
