#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Fri May  4 21:22:16 2018

@author: scottvanderleeden
"""

import numpy as np


def envelope(x, fs, tauA, tauR, blockLength, processWidth):
    env0 = np.zeros(processWidth)
    env = np.zeros_like(x)
    alphaA = np.exp(-1/(tauA*fs))
    alphaR = np.exp(-1/(tauR*fs))
    i = 0
    # initilising alpha array
    alpha = np.zeros(processWidth)

    while (i <= blockLength-1):
        x_abs = np.abs(x[:, i])
        if (i == 0):
            y0 = env0
        else:
            y0 = env[:, i-1]
        # Using np.where instead of if to set both channels
        aIdx = np.where(x_abs > y0)
        rIdx = np.where(x_abs <= y0)
        """
        Old method using if statement for each channel
        if ( x_abs[0] > y0[0] ):
            alpha[0] = alphaA
        else:
            alpha[0] = alphaR
        # right channel
        if ( x_abs[1] > y0[1] ):
            alpha[1] = alphaA
        else:
            alpha[1] = alphaR
        """
        alpha[aIdx] = alphaA
        alpha[rIdx] = alphaR
        env[:, i] = alpha * y0 + (1 - alpha) * x_abs
        env0 = env[:, i-1]
        i += 1
    """    # plot attempts
    py.figure()
    py.plot(x, label='signal')
    py.plot(env, lebel='envelope')
    py.legend(loc=0)
    """
    return(env)
