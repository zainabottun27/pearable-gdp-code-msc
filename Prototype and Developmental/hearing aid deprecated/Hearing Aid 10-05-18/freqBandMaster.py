#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Thu May  3 22:18:38 2018

@author: scottvanderleeden
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 15:16:29 2018

@author: pi
This script was written in order to plot the offline results of the functions
to be run in real-time using VISR
"""
import octaveBandFilt
import numpy as np
from scipy.io import wavfile
from matplotlib import pylab as plt
import scipy.signal as signal


# Offline tests of compression and envelope algorithms


def offlineHA(bands, wavName, order=2, width=2, writeName=None, seconds=None):
    """
    Performs the octave band filterinf on the wav file provided.
    If a value for seconds is given, the computation will be done that many
    seconds on the wavfile, if not then computation will be done on the whole
    wav file
    Parameters
    ----------
    bands: ndarry
        array of centre frequencies e.g. np.array([250, 500, 1k, 2k, 4k])
    wavname: string
        name of the wavfile to read (no need to include '.wav' in the name)
    seconds: integar
        if given, the wav file will only run for this length of time
        (default: None)
    order: integar
        the order of the filters being used to octave band filter the data
        (default: 2)
    width: integar
        number of channels of the input signal/wav file (default: 2)
    writeName: string
        if given the wavfile will be saved using that name, if not given the
        wavfile will be saved as the file name with 'HearingAided' added to the
        end (no need to include '.wav' in the name)

    Returns
    -------
    filt: ndarry
        data after being filtered in to octave bands, 
        size(width*number of octave bands) the data will containt the left
        channel octave bands fopllowed by the right chasnnel octave bands
    t: ndarray
        time vector for plotting
    data: ndarry
        the input data found from the wav file
    fs: integar
        sampling frequency used
    """
    # print the bands being used
    print('Bands used: '+str(bands))
    # read the wavfile
    fs, data = wavfile.read(wavName+'.wav')
    # make the data the correct shape for the hearing aid functions
    data = data.reshape([data.shape[1], data.shape[0]])
    # if statements to decide what to do
    # if oneBuffer is True only use one buffer of data
    if seconds is None:
        pass
    # if seconds are given make the data the correct number of seconds long
    else:
        # seconds*fs gives the number of samples to use
        data = data[:, 0:seconds*fs]
    # create the correct time vector based on the sampling frequency
    t = np.arange(0, data.shape[1])/float(fs)
    # calculate the allowed time for the processing for realtime application
    # use octaveBandFilt to filter the signal into octave bands
    # octaveBandFilt.getFilterCoeffs() gets the filter coefficients from the
    # sampling frequency, an array of band centre frequencies and the filter
    # order to use
    b, a = octaveBandFilt.getFilterCoeffs(fs, bands, order)
    # octaveBandFilt.applyFilterCoeffs() applies the filter coefficients found
    # given the data to filter dataFS, b, a the number of channels
    # width, the buffer size and the array of band centre frequencies
    filt = octaveBandFilt.applyFilterCoeffs(data, b, a, width,
                                            bufferSize=data.shape[1],
                                            bands=bands)
    # make the output data the correct shape for a wavfile
    outWav = filt.reshape([filt.shape[1], filt.shape[0]])
    # if no writeName is given, make one
    if writeName is None:
        # add HearingAided.wav to the end of the wavfile name
        writeName = wavName+str('Filtered.wav')
    # if writeName is given add .wav to the end
    else:
        writeName = writeName+str('.wav')
    # writes the normalised output data to a wavfile
    wavfile.write(writeName, fs, outWav/np.max(outWav))
    # return the relavent variables
    return(filt, t, data, fs, b, a)


# choose a wavfile to test. 'snarky.wav' is a 2ch 48000 fs file
testFilename = 'whiteNoise'
# choose which bands to use byu indexing them
bands = np.array([125, 250, 500, 1000, 2000, 4000, 8000, 16000])
# perform the offline test
filt, t, data, fs, b, a = offlineHA(bands, testFilename, order=2, width=2,
                              writeName=None, seconds=None)
# %%
plt.close('all')
plt.rcParams['figure.figsize']=(18., 16.)

fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True)

if len(bands)==1:
    w, h = signal.freqz(b, a)
    # gets the frequency response of the filter h and the freq w in rad/s
    f = w/(2*np.pi)*fs  # converts w to Hz
    ax1.semilogx(f, 20*np.log10(abs(h)), label=str(bands[0])+' Hz')
else:
    for i in range(len(bands)):
        w, h = signal.freqz(b[i, :], a[i, :])
        # gets the frequency response of the filter h and the freq w in rad/s
        f = w/(2*np.pi)*fs  # converts w to Hz
        notZero = np.where(h!=0)
        H = np.zeros_like(h, dtype=np.float)
        H[notZero] = 20*np.log10(np.abs(h[notZero]))
        ax1.semilogx(f, H, label=str(bands[i])+' Hz')


bothChan = False
f = np.linspace(0, fs, len(filt[1]))
for i in range(len(bands)*2):
    if i < len(bands):
        FILT = np.abs(np.fft.fft(filt[i, :]))
        ax2.semilogx(f, FILT/max(FILT), label='Left '+str(bands[i])+' Hz',
                 alpha=0.7)
    else:
        if bothChan:
            FILT = np.abs(np.fft.fft(filt[i, :]))
            ax2.semilogx(f, FILT/max(FILT),
                         label='Right '+str(bands[i-len(bands)]) +' Hz',
                         alpha=0.7)

dataRS = data.reshape([data.shape[1], data.shape[0]])
DATA = np.abs(np.fft.fft(dataRS))
ax3.plot(f, DATA/np.max(DATA), alpha=0.7)

ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))
ax2.legend(loc='center left', bbox_to_anchor=(1, 0.5))
ax3.legend(['Left', 'Right'], loc='center left', bbox_to_anchor=(1, 0.5))
ax1.set(ylim=[-120, 0], ylabel='Gain (dB)', title='Filter FRFs')
ax2.set(ylabel='Normalised Magnitude', title='Filtered .wav File')
ax3.set(xlabel='Frequency (Hz)', ylabel='Normalised Magnitude',
        title='.wav file data')
plt.xlim([10, fs/2])
