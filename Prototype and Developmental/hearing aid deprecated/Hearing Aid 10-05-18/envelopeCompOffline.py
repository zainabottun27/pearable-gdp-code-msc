#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 University of Southampton - GDP Group 16

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

 # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Created on Fri Mar 23 15:16:29 2018

@author: pi
This script was written in order to plot the offline results of the functions
to be run in real-time using VISR
"""

import envelopeFunction
import envelopeControl
import compressionFunction1 as compressionFunction
import compressionInterp
import compressionFunctionInterp
import noiseReduction as NR
import gainFunction
import octaveBandFilt
import numpy as np
from scipy.io import wavfile
import time
import hearingAidVariables as HA
import freqShiftFunction as FS
import octaveSum as OS
from matplotlib import pylab as plt


# Boolean feed through choice for envelope
fTEnvelope = False
fTCompression = False

def readwav(wavName):
    """
    """
    # find the relavent hearing aid variables from the band index
    # read the wavfile
    fs, x = wavfile.read(wavName+'.wav')
    # create the correct time vector based on the sampling frequency
    if len(x[0, :]) > 1:
        return(x.T, fs)
    else:
        datanew = np.zeros([2, len(data)])
        datanew[0, :] = x
        datanew[1, :] = x
        x = datanew
    return(x, fs)


def offlineHA(data, fs, tauA, tauR, T, CR, KW, MG, LK, UK, width, hilbert=True,
              name1='default', name2='default'):
    if hilbert:
        ev = envelopeFunction.envelope(data, feedThrough=fTEnvelope)
    else:
        ev = envelopeControl.envelope(data, fs, tauA, tauR, data.shape[1],
                                      processWidth=2, feedThrough=fTEnvelope)
    
#    com = compressionFunction.compress(data, ev, T, CR, KW, MG,
#                                       feedThrough=fTCompression)
    com = compressionFunctionInterp.compress(data, ev, T, CR, LK, UK, MG, width,
                                             feedThrough=fTCompression, name1=name1,
                                             name2=name2)
    return(com, ev, data, fs)

## %%
## tauA = np.array([[0.005, 0.005], [0.05, 0.05], [0.005, 0.005]])
## tauR = np.array([[0.05, 0.05], [0.25, 0.25], [0.07, 0.07]])
#tauA = np.array([[0.005, 0.005]])
#tauR = np.array([[0.07, 0.07]])
#T = np.array([0.8, 0.8])
#CR = np.array([0.8, 0.8])
#KW = np.array([1, 1])
#MG = np.array([0, 0])
## choose a wavfile to test. 'snarky.wav' is a 2ch 48000 fs file
#testFilename = 'noiseySpeech'
## perform the offline test
#data, fs = readwav(testFilename)
#ev = np.zeros([len(tauA[:, 0]), data.shape[1]])
#comp = np.zeros([len(tauA[:, 0]), data.shape[1]])
#for i in range(len(tauA[:, 0])):
#    com, env, t, data, fs = offlineHA(data, fs, tauA=tauA[i, :],
#                                      tauR=tauR[i, :], T=T, CR=CR,
#                                      KW=KW, MG=MG)
#    ev[i, :] = env[0, :]
#    comp[i, :] = com[0, :]
## %%
#t = np.arange(0, data.shape[0])/float(fs)
#dataPlot = data.T
#envPlot = ev.T
#comPlot = comp.T
#plt.close('all')
#plt.figure()
#plt.plot(t, dataPlot[:, 0])
#for i in range(len(envPlot[0, :])):
#    plt.plot(t, envPlot[:, i],
#             label=('Attack time: %.f ms\nReleaseTime: %.f ms'
#                    % (tauA[i][0]*1000, tauR[i][0]*1000)))
#plt.legend()
#
#plt.figure()
#plt.plot(t, dataPlot[:, 0])
#for i in np.arange(len(envPlot[0, :])-1, -1, -1):
#    plt.plot(t, comPlot[:, i],
#             label=('Attack time: %.f ms\nReleaseTime: %.f ms'
#                    % (tauA[i][0]*1000, tauR[i][0]*1000)), alpha=0.5)
#plt.legend()

# %%

plt.close('all')
plt.rcParams['figure.figsize'] = (18., 16.)
plt.rcParams['font.size'] = (16.)
Sine = False
hilbert = True
saveEnv = False
saveComp = False
if Sine:
    fs = 48000
    T = 0.2
    N = T*fs
    t = np.linspace(0, T, N)
    f1 = 1000
    f2 = 10
    sine = (np.sin(2*np.pi*f1*t)*(np.sin(2*np.pi*f2*t)+2))
    plt.figure()
    plt.plot(t, sine)
    data = np.array([sine, sine])
else:
    # choose a wavfile to test
    testFilename = 'SpeechLeftRight'
    # perform the offline test
    data, fs = readwav(testFilename)
    t = np.arange(0, data.shape[1])/float(fs)

width = 2
# tauA = np.array([[0.005, 0.005]])
# tauR = np.array([[0.07, 0.07]])
tauA = np.array([[0.001, 0.001], [0.005, 0.005], [0.008, 0.008]])
tauR = np.array([[0.05, 0.05], [0.02, 0.02], [0.02, 0.02]])
T = np.array([20, 20])
CR = np.array([2, 2])
KW = np.array([80, 80])
MG = np.array([12, 12])
UK = np.array([70, 70])
LK = np.array([30, 30])



channel = 1
ev = np.zeros([len(tauA[:, channel]), data.shape[1]])
comp = np.zeros([len(tauA[:, channel]), data.shape[1]])

for i in range(len(tauA[:, channel])):
    if hilbert:
        if Sine:
            com, env, data, fs = offlineHA(data, fs, tauA=tauA[i, :],
                                           tauR=tauR[i, :], T=T, CR=CR,
                                           KW=KW, MG=MG, LK=LK, UK=UK, width=width,
                                           hilbert=True, name1='InOutHilbertSine.png',
                                           name2='InGainHilbertSine.png')
        else:
            com, env, data, fs = offlineHA(data, fs, tauA=tauA[i, :],
                                           tauR=tauR[i, :], T=T, CR=CR,
                                           KW=KW, MG=MG, LK=LK, UK=UK, width=width,
                                           hilbert=True, name1='InOutHilbert.png',
                                           name2='InGainHilbert.png')
        ev[i, :] = env[channel, :]
        comp[i, :] = com[channel, :]
    else:
        if Sine:
            com, env, data, fs = offlineHA(data, fs, tauA=tauA[i, :],
                                           tauR=tauR[i, :], T=T, CR=CR,
                                           KW=KW, MG=MG, LK=LK, UK=UK, width=width,
                                           hilbert=False, name1='InOutSine.png',
                                           name2='InGainSine.png')
        else:
            plt.figure()
            com, env, data, fs = offlineHA(data, fs, tauA=tauA[i, :],
                                           tauR=tauR[i, :], T=T, CR=CR,
                                           KW=KW, MG=MG, LK=LK, UK=UK, width=width,
                                           hilbert=False, name1='InOut.png',
                                           name2='InGain.png')
            
        ev[i, :] = env[channel, :]
        comp[i, :] = com[channel, :]

dataPlot = data.T
envPlot = ev.T
comPlot = comp.T

if hilbert:
    labelEnv = 'Envelope'
    labelComp = 'Compressed Signal'



fig, (ax1) = plt.subplots(1, sharex=True)
ax1.plot(t, dataPlot[:, channel], label='Input')
if hilbert:
    ax1.plot(t, envPlot[:, 0], label=labelEnv)
else:
    for i in np.arange(len(envPlot[0, :])-1, -1, -1):
        labelEnv = ('Attack time: %.f ms\nReleaseTime: %.f ms'
                 % (tauA[i][channel]*1000, tauR[i][channel]*1000))
        ax1.plot(t, envPlot[:, i], label=labelEnv)
ax1.set(xlabel='Time (s)', ylabel='Magnitude (a.u.)')
# plt.xlim([9, 12])
ax1.legend()
if saveEnv:
    if hilbert:
        if Sine:
            plt.savefig('EnvelopeHilbertSine.png')
        else:
            plt.savefig('EnvelopeHilbert.png')
    else:
        if Sine:
            plt.savefig('EnvelopeChangeTauSine.png')
        else:
            plt.savefig('EnvelopeChangeTau.png')


fig, (ax2) = plt.subplots(1, sharex=True)
ax2.plot(t, dataPlot[:, channel], label='Input')
if hilbert:
    ax2.plot(t, comPlot[:, 0],
             label=labelComp, alpha=0.7)
else:
    for i in np.arange(len(envPlot[0, :])-1, -1, -1):
        labelComp = ('Attack time: %.f ms\nReleaseTime: %.f ms'
                     % (tauA[i][channel]*1000, tauR[i][channel]*1000))
        ax2.plot(t, comPlot[:, i], label=labelComp, alpha=0.7)
ax2.set(xlabel='Time (s)', ylabel='Magnitude (a.u.)')
# plt.xlim([9, 12])
# plt.ylim([-2, 2])
ax2.legend()
if saveComp:
    if hilbert:
        if Sine:
            plt.savefig('CompressionHilbertSine.png')
        else:
            plt.savefig('CompressionHilbert.png')
    else:
        if Sine:
            plt.savefig('CompressionChangeTauSine.png')
        else:
            plt.savefig('CompressionChangeTau.png')
# %%
            plt.close('all')
plt.figure()
plt.plot(t, comPlot[:, 0])
plt.figure()
from scipy import signal
fsS = fs
nperseg=64
noverlap=nperseg/8
comPlotLog = np.zeros_like(comPlot)
notzero = np.where(comPlot>1)
comPlotLog[notzero] = 20*np.log10(comPlot[notzero])
f, ts, Sxx = signal.spectrogram(comPlotLog[:, 0], fs=fsS, nperseg=nperseg, noverlap=noverlap)
plt.pcolormesh(ts, f, Sxx)
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')
plt.show()
