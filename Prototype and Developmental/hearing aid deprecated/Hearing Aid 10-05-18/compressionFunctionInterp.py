#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 10 16:29:21 2018

@author: scottvanderleeden
"""
import numpy as np
from scipy import interpolate


def compress(x, env, T, CR, LK, UK, MG, width, feedThrough=False, name1=None,
             name2=None):
    """
    Compresses a signal given the envelope and other compression parameters
    Parameters
    ----------
    x: ndarray
        The signal that will be compressed
    env: ndarray
        The envelope of x (must have same size as x)
    Hearing aid parameters:
    The following parameters are arrays of floats to be applied to each octave
    band in two channels (left and right ears), they have length up to 10 for 2
    channels and 5 octave bands
    T: ndarray
        compression thresholds
    CR: ndarry
        compression ratios
    KW: ndarry
        knee widths
    MG: ndarray
        make-up gains
    feedThrough: Boolean
        if True feed through audio only

    Returns
    -------
    y: ndarray
        the compressed signal
    """
    if feedThrough:
        return(x)
    envdB = np.zeros_like(env)
    nonzero = np.where(env > 1)
    envdB[nonzero] = 20*np.log10(env[nonzero])
    maxSig = 200
    In = np.array([np.zeros(width), LK, UK, np.linspace(maxSig, maxSig,
                   width)])
    Out = np.array([np.zeros(width), LK, np.multiply((1./CR), (UK-LK))+LK,
                    np.multiply((1./CR), (UK-LK))+LK])
    OutEnv = np.zeros_like(env)
    for i in range(width):
        f = interpolate.interp1d(In[:, i], Out[:, i])
        OutEnv[i, :] = f(envdB[i, :])
    G = OutEnv-envdB
    g = np.power(10, (G/20))
    mg = np.power(10, (MG/20))[:, None]
    y = np.multiply(g, x)*mg
    return(y)
