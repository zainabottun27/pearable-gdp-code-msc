#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  9 12:30:45 2018

@author: pi
"""

import numpy as np
from scipy import signal
import time

def calculate(x, fs, bands, order):
    filtData = np.zeros([x.shape[0], x.shape[1], len(bands)])
    fD = 2**0.5
    fU, fL = bands*fD, bands/fD  # gets the lower and upper octave band freqs
    Wn = np.array([fL, fU])/(0.5*fs)
    timeStart = time.time()
    for i in range(len(bands)):
        sos = signal.butter(order, Wn[:,i], btype='band', output='sos')
        filtData[:,:,i] = signal.sosfilt(sos, x)
#    print('For loop in filter function takes %f seconds ' % (time.time()-timeStart))
    # Reshaping array to be 2D for VISR, 10x512
    # first 5 cols will be left channel data for the 5 octave bands, next 5
    # for the right
    timeStart=time.time()
    filtData=filtData.reshape([x.shape[0]*len(bands), x.shape[1]]) # 10x512
#    print('Reshape in filter function takes %f seconds ' % (time.time()-timeStart))
    return(filtData)