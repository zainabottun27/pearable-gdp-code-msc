x = load('latency.txt');
fs = x(end);
period_size = x(end-1);
x(end-1:end) = [];
T = period_size/fs;
Tms = T*1000;

t = (0:length(x)-1)*T

plot(t,x*1000)
xlabel('Time (s)')
ylabel('Latency (ms)')
xlim([0 max(t)])
ylim([0 Tms])