#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 12:27:44 2018

@author: pi
"""

import visr
import gain_python, compression_function, envelope_function, filtFreq_function, filtFreq_function_fast
import time
import numpy as np
from scipy import signal

class feedThrough(visr.CompositeComponent):
    """ Adder for the gain function """
    def __init__(self, context, name, parent, width, G, T, CR,
                 KW, MG, order, bands):
        super(feedThrough, self).__init__(context, name, parent)
        self.input = visr.AudioInputFloat("in", self, width)
        self.output = visr.AudioOutputFloat("out", self, width)
        
        self.audioConnection(self.input, self.output)
        
    #        

class Pearable2(visr.CompositeComponent):
    """ Adder for the gain function """
    def __init__(self, context, name, parent, width, G, T, CR,
                 KW, MG, order, bands):
        super(Pearable2, self).__init__(context, name, parent)
        processWidth = width*len(bands)  # width of processed data 10 
        self.sum = octSum(context, "Adder", self, width, processWidth)
        self.input = visr.AudioInputFloat("in", self, width)
#        self.octBands = filtFreq(context, "bands", self, width, processWidth, bands, order)
        self.octBands = filtFreqFast(context, "bands", self, width, processWidth, bands, order)
        self.output = visr.AudioOutputFloat("out", self, width)
        self.env = envelope(context, "e", self, processWidth, bands)
        self.gain = gain_function(context, "g", self, processWidth, G)
            
        self.comp = compression(context, "c", self, processWidth, T,CR,KW,MG)
        
        self.audioConnection(self.input, self.octBands.audioPort("in"))
#        self.gain = gain_function(context, "g%d" % i, self, width, G[i])
#            
#        self.comp = compression(context, "c%d" % i, self, width, T[i],CR[i],KW[i],MG[i])



        #        self.sum = octSum(context, "Adder", self, width)
        #        self.oct250 = filtFreq(context, "250", self, width, 250, order)
        #        self.oct500 = filtFreq(context, "500", self, width, 500, order)
        #        self.oct1k = filtFreq(context, "1000", self, width, 1000, order)
        #        self.oct2k = filtFreq(context, "2000", self, width, 2000, order)
        #        self.oct4k = filtFreq(context, "4000", self, width, 4000, order)
            
#            self.allowedTime = float(self.period())/self.samplingFrequency()
        #        self.freqSum = PythonAdder(context, "Adder", self, 2, width )
            
            
        #        
        ##        
        #        self.audioConnection(self.input, self.oct250.audioPort("in"))
        ##        self.audioConnection(self.oct250.audioPort("out"), self.output)
        #        
        #        self.audioConnection(self.input, self.oct2k.audioPort("in"))
        #        self.audioConnection(self.oct250.audioPort("out"), self.freqSum.audioPort("in0"))
        #        
        #        self.audioConnection(self.oct2k.audioPort("out"), self.freqSum.audioPort("in1"))
        #        
        #        self.audioConnection(self.freqSum.audioPort("out"), self.output) 
        #        



       
        self.audioConnection(self.octBands.audioPort("out"), self.env.audioPort("in"))
        self.audioConnection(self.octBands.audioPort("out"), self.comp.audioPort("in"))
        self.audioConnection(self.env.audioPort("out"), self.comp.audioPort("env"))
        self.audioConnection(self.comp.audioPort("out"), self.gain.audioPort("in"))
        self.audioConnection(self.gain.audioPort("out"), self.sum.audioPort("in"))
        
        
        self.audioConnection(self.sum.audioPort("out"), self.output)
        
    #        
#        t_start = time.time()
#        
##        self.audioConnection(self.input, self.oct250.audioPort("in"))
#        print(self.input.width, self.octBands.audioPort("in").width)
#        
#        
#        envTime = time.time()-t_start
#        print("Envelope takes %f seconds of %f" % (envTime, self.allowedTime))
#        t_start1 = time.time()
##        self.audioConnection(self.comp.audioPort("out"), self.output)
#        compTime = time.time()-t_start1
#        print("Compression takes %f seconds of %f" % (compTime,
#                                                      self.allowedTime))
#        
#       
#        t_start1 = time.time()
#        
#        
#        
#        
#        gainTime = time.time()-t_start1
#        print("Compression takes %f seconds of %f" % (gainTime,
#                                                      self.allowedTime))
#        print("VISR takes %f seconds of %f; sum: %f" % (time.time()-t_start,
#                                                        self.allowedTime,
#                                                        compTime+envTime))




            
    #        self.audioConnection(self.input, self.env.audioPort("in"))
    #        self.audioConnection(self.env.audioPort("out"), self.gain.audioPort("in"))
    ##        self.audioConnection(self.gain.audioPort("out"), self.output)
    #        self.audioConnection(self.input, self.gain.audioPort("in"))
    #        self.audioConnection(self.gain.audioPort("out"), self.output)
    
    #        self.audioConnection(self.input [0], self.env.audioPort("in"), [0])
    #        self.audioConnection(self.env.audioPort("out"), [0], self.comp.audioPort("in"), [0])
    #        self.audioConnection(self.comp.audioPort("out"), [0], self.gain.audioPort("in"), [0])
    #        self.audioConnection(self.gain.audioPort("out"), [0], self.output, [0])
    #        
    #        self.audioConnection(self.input [1], self.env.audioPort("in"), [1])
    #        self.audioConnection(self.env.audioPort("out"), [1], self.comp.audioPort("in"), [1])
    #        self.audioConnection(self.comp.audioPort("out"), [1elf.env.audioPort("o], self.gain.audioPort("in"), [1])
    #        self.audioConnection(self.gain.audioPort("out"), [1], self.output, [1])
        
class filtFreq(visr.AtomicComponent):
    def __init__( self, context, name, parent, width, processWidth, bands, order):
        super(filtFreq,self).__init__( context, name, parent )
        self.input = visr.AudioInputFloat( "in", self, width )
        self.output = visr.AudioOutputFloat("out", self, processWidth)
        self.bands = bands
        self.order = order
        # self.sos array of sos coefficients only needs to be defined once
    def process( self ):
        x = self.input.data()
                # 3D filtered data
        y = filtFreq_function.calculate(x, self.samplingFrequency(), self.bands, self.order)
        self.output.set(y)


#    def __init__(self, context, name, parent, numInputs, width, band, order):
#        super(filtFreq, self).__init__(context, name, parent)
#        self.input = visr.AudioInputFloat("in", self, width)
#        self.output = visr.AudioOutputFloat("out", self, width)
#        self.numInputs = numInputs
#        self.band = band
#        self.order = order
#    def process(self):
#        x = self.input.data()
#        y = filtFreq_function.calculate(x, self.samplingFrequency(), self.band, self.order)
#        print(y.shape)
#        self.output.set(y)
#        class filtFreq(visr.AtomicComponent):


class filtFreqFast(visr.AtomicComponent):
    def __init__( self, context, name, parent, width, processWidth, bands, order):
        super(filtFreqFast,self).__init__( context, name, parent )
        self.input = visr.AudioInputFloat( "in", self, width )
        self.output = visr.AudioOutputFloat("out", self, processWidth)
        self.bands=bands
        self.width=width
        self.b, self.a = filtFreq_function_fast.getFilterCoeffs(self.samplingFrequency(),bands, order)
    def process( self ):
        x = self.input.data()
        y = filtFreq_function_fast.calculate(x, self.b, self.a, self.width, self.period(), self.bands)
        self.output.set(y)

class gain_function(visr.AtomicComponent):
    """ Adder for the gain function """
    def __init__(self, context, name, parent, processWidth, G):
        super(gain_function, self).__init__(context, name, parent)  
        self.input = visr.AudioInputFloat( "in", self, processWidth )
        self.output = visr.AudioOutputFloat("out", self, processWidth)
        self.G = G

    def process(self):
        x = self.input.data()
#       y = self.output.data()
        y = gain_python.calculate(x, self.G)
        self.output.set(y)
#        self.output.set(gain_python.calculate(x, self.G, self.period(), y))


class envelope(visr.AtomicComponent):
    def __init__(self, context, name, parent, processWidth, bands):
        super(envelope, self).__init__(context, name, parent)
        self.input = visr.AudioInputFloat( "in", self, processWidth )
        self.output = visr.AudioOutputFloat("out", self, processWidth)
    def process(self):     
        x = self.input.data()
#           y = self.output.data()
        y = envelope_function.calculate(x)
        self.output.set(y)
#        self.output.set(gain_python.calculate(x, self.G, self.period(), y))
#class envelope(visr.AtomicComponent):
#    """ Adder for the gain function """
#    def __init__(self, context, name, parent, width, tauA, tauR, env, T, CR, KW, MG):
#        super(envelope, self).__init__(context, name, parent)
#        self.input = visr.AudioInputFloat("in", self, width)
#        self.output = visr.AudioOutputFloat("out", self, width)
#        self.tauA, self.tauR,self.env = tauA, tauR, env
#        self.width = width  # num channels
#
#    def process(self):
#        x = self.input.data()
#        y = envelope_function.calculate(x,self.samplingFrequency(),self.tauA,
#                                         self.tauR,self.period(),self.env,
#                                         self.width)
#        self.output.set(y)
##        self.output.set(gain_python.calculate(x, self.G, self.period(), y))

#class compression(visr.AtomicComponent):
#    """ Adder for the gain function """
#    def __init__(self, context, name, parente, width, env,T,CR,KW,MG):
#        super(compression, self).__init__(context, name, parent)
#        self.input = visr.AudioInputFloat("in", self, width)
#        self.output = visr.AudioOutputFloat("out", self, width)
#        self.env,self.T,self.CR,self.KW,self.MG = env,T,CR,KW,MG
#  # num channels
#
#    def process(self):
#        x = self.input.data()
#        y = self.output.data()
#        y = compression_function.calculate(x,self.period(),self.env,self.T,self.CR,self.KW,self.MG,y)
#        self.output.set(y)
##        self.output.set(gain_python.calculate(x, self.G, self.period(), y))


class compression(visr.AtomicComponent):
    """ Adder for the gain function """
    def __init__(self, context, name, parent, processWidth,T,CR,KW,MG):
        super(compression, self).__init__(context, name, parent)   
        self.input = visr.AudioInputFloat("in", self, processWidth)
        self.env = visr.AudioInputFloat("env", self, processWidth)
        self.output =visr.AudioOutputFloat("out", self, processWidth)
        self.T,self.CR,self.KW,self.MG = T,CR,KW,MG
        # num channels

    def process(self):
        x = self.input.data()
        e = self.env.data()
#       y = self.output.data()
        y = compression_function.calculate(x,e,self.T,self.CR,self.KW,self.MG)
        self.output.set(y)


class octSum( visr.AtomicComponent ):
    """ General-purpose add block for an arbitrary number of inputs"""
    def __init__( self, context, name, parent, width, processWidth):
        super(octSum, self).__init__( context, name, parent )
        self.input = visr.AudioInputFloat("in", self, processWidth)
        self.output = visr.AudioOutputFloat( "out", self, width )
        self.width = width
    def process( self ):
        x = self.input.data()
        data_3d = x.reshape([self.width, x.shape[1], int(x.shape[0]/self.width)])
        y = np.sum(data_3d, axis=2)
        self.output.set(y)


class PythonAdder( visr.AtomicComponent ):
    """ General-purpose add block for an arbitrary number of inputs"""
    def __init__( self, context, name, parent, numInputs, width ):
        super(PythonAdder,self).__init__( context, name, parent )
        self.output = visr.AudioOutputFloat( "out", self, width )
        self.inputs = []
        for inputIdx in range( 0, numInputs ):
            portName =  "in%d" % inputIdx
            inPort = visr.AudioInputFloat( portName, self, width )
            self.inputs.append( inPort )
    def process( self ):
        if len( self.inputs ) == 0:
            self.output.set( np.zeros( (self.output.width, self.period ), dtype = np.float32 ) )
        elif len( self.inputs ) == 1:
            self.output.set( np.array(self.inputs[0]))
        else:
            acc = np.array(self.inputs[0])
            for idx in range(1,len(self.inputs)):
                acc += np.array(self.inputs[idx])
            self.output.set( acc )
