#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 01:56:22 2018

@author: pi
"""

import VISRComponents
import visr
import rrl
import audiointerfaces as ai
import time
import numpy as np
import HearingAidVariables as HA
width = 2
fs = 32000  # sampling frequency (JACK interface fs set to match this)
blockSize = 2**12#int(512*3)  # block/buffer size 512
allowedTime = blockSize/(float(fs))
order = 2
bandIndex = np.array([4])
bands, G, T, CR, KW, MG = HA.numBands(bandIndex)

c = visr.SignalFlowContext(blockSize, fs)  # sets the sampling frequency and
# buffer size in VISR
pa1 = VISRComponents.Pearable2(c, "pa0", None, width, G, T, CR,
                              KW, MG, order, bands) 
flow = rrl.AudioSignalFlow(pa1)
# the component containing the processing functionality
aiConfig = ai.AudioInterface.Configuration(flow.numberOfCaptureChannels,
                                           flow.numberOfPlaybackChannels,
                                           fs,
                                           blockSize)

jackCfg = """{ "clientname": "Pearable2",
  "autoconnect" : "true",
  "portconfig":
  {
    "capture":  [{ "basename":"in_", "externalport" : {} }],
    "playback": [{ "basename":"out_", "externalport" : {} }]
  }
}
"""

#aIfc = ai.AudioInterfaceFactory.create("Jack", aiConfig, jackCfg)
aIfc = ai.AudioInterfaceFactory.create("PortAudio", aiConfig, """{"hostapi":"ALSA"}""")

#aIfc = ai.AudioInterfaceFactory.create("PortAudio", aiConfig, jackCfg)

aIfc.registerCallback(flow)
#aIfc1.registerCallback(flow1)

start_time = time.time()
aIfc.start()
#aIfc1.start()

print("Allowed Time {}".format(allowedTime))
print("Rendering started, aIfc start time:. {}".format(time.time()-start_time))


time.sleep(5)
i = input("Enter text (or Enter to quit): ")
if not i:
    aIfc.stop()
    aIfc.unregisterCallback()
    del aIfc
