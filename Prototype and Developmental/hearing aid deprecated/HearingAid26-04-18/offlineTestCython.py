#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 10:03:00 2018

@author: pi
"""

import time

import pyximport

pyximport.install()

import envelope_function_2_opt
import envelope_function
import compression_function
from scipy.io import wavfile
import numpy as np
import pylab as py
#%%
testFilename = 'snarky.wav' # this is 2ch 48000 fs file
width = 2.
fs = 44100.  # sampling frequency (JACK interface fs set to match this)
G = -6  # dB gain for the gain function gain_python
tauA, tauR = 0.005, 0.02
T, CR, KW, MG = 0., 1., 10., 0.
fs, data=wavfile.read(testFilename)
blockLength = 512
timeAvailable = fs/blockLength
data=data[0:blockLength]
data = data.reshape([data.shape[1], data.shape[0]])  # swapping order of data
env = np.zeros([np.int(width), np.int(blockLength)])
env0 = np.zeros(np.int(width))
t = np.arange(0, float(blockLength))/float(fs)
y=np.zeros_like(data)

#%%
t_start = time.time()
ev=envelope_function.calculate(data, fs, tauA, tauR, blockLength,
                               env,np.int(width))
print("Python envelope code needs %f seconds" % (time.time()-t_start))


#t_start = time.time()
#ev_opt=envelope_function_2_opt.calculate(data, np.int(fs), np.int(tauA), np.int(tauR), np.int(blockLength),
#                               env, np.int(width), env0)
#print("Cython envelope code needs %f seconds" % (time.time()-t_start))
#

# %%
t_start = time.time()
com=compression_function.calculate(data,blockLength,ev,T,CR,KW,MG,data)
print("Python compression code needs %f seconds" % (time.time()-t_start))

com = com.reshape([com.shape[1], com.shape[0]])  # swapping order of data
wavfile.write('snarkyCompressed', fs, com)

# %% Plotting - envelope
py.figure()
py.plot(t, data[0],color='C0', ls='-', label='wav file - L')
py.plot(t, data[1],color='C0', ls='--', label='wav file - R')
py.plot(t, ev[0],color='C1', ls='-', label='Envelope - L')
py.plot(t, ev[1],color='C1',ls='--', label='Envelope - R')
py.legend(loc=0)
py.xlabel(r'$Time - [s]$')
py.ylabel(r'$Amplitude - [a.u.]$')
py.title('Offline processing of Signal Evelope of {} File'.format(testFilename))
py.savefig('EnvelopePlot')

## %% Plotting - compression
py.figure()
py.plot(t, data[0],color='C0', ls='-',label='wav file - L')
py.plot(t, data[1],color='C0', ls='--',label='wav file - R')
py.plot(t, com[0],color='C1', ls='-',label='Compressed - L')
py.plot(t, com[1],color='C1', ls='--',label='Compressed - R')
py.legend(loc=0)
py.xlabel(r'$Time - [s]$')
py.ylabel(r'$Amplitude - [a.u.]$')
py.title('Offline processing of Compression of {} File'.format(testFilename))
py.savefig('CompressionPlot')