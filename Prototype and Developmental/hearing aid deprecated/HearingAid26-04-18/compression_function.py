import numpy as np

def calculate(x,env,T,CR,KW,MG):
    # setting up array for ydB, xdB envdB
    ydB=np.zeros_like(x)
    xdB = np.zeros_like(x)
    envdB = np.zeros_like(env)
    T_T, KW_T, MG_T = T[:,None], KW[:,None], MG[:,None]
#        zeroIdx = np.where(x[:,i]==0) # zero idx for dB cannot get inf value
    nonZeroIdx = np.where(x!=0)
    nonZeroEnvIdx = np.where(env!=0)
    # want to only calc dB of nonzero values
    xdB[nonZeroIdx] = 20*np.log10(np.abs(x[nonZeroIdx]))
    envdB[nonZeroEnvIdx] = 20*np.log10(np.abs(env[nonZeroEnvIdx]))
    # Using np.where to set each channel, instead of if statements
    # constant gain region of graph
    cstGainIdx = np.where(2*(envdB-T_T)< -KW_T)
    # Compression region of graph
    compressionIdx = np.where((2*np.abs(envdB-T_T))<=KW_T)
    # limiting region of graph
    limitingIdx = np.where((2*(envdB-T_T)) > KW_T)
    # Setting ydB indicies to be the approprite gain      
#        ydB[:,cstGainIdx] = xdB[cstGainIdx]
    ydB[cstGainIdx] = xdB[cstGainIdx]        
#        ydB[:,compressionIdx] = xdB[compressionIdx]+(1/CR-1)*((
#                xdB[compressionIdx]-T+(KW/2))**2)/(2*KW)
#    print(compressionIdx, xdB, ydB)    
#    print(ydB[compressionIdx].shape, xdB[compressionIdx].shape)
    ydB[compressionIdx] = xdB[compressionIdx]+(1/CR[compressionIdx[0]]-1)*(\
       (xdB[compressionIdx]-T[compressionIdx[0]]+(KW[compressionIdx[0]]/2))\
       **2)/(2*KW[compressionIdx[0]])
#        ydB[:,limitingIdx] = T+((xdB[limitingIdx]-T)/CR)
    ydB[limitingIdx] = T[limitingIdx[0]]+((xdB[limitingIdx]-T[limitingIdx[0]])/CR[limitingIdx[0]])
    # Adding markup gain
    ydB = ydB + MG_T
    # converting to linear
#        y = 10**(ydB/20)
#        y[:, i] = 10**(ydB[:, i]/20)
##        y=0
#        y[zeroIdx, i]=0
#        # getting negative idicies to convert back to negative
#        negIdx = np.where(x<0)
##        negIdx = np.where(x[:,i]<0)
#        # Setting to be negative
#        y[negIdx] = -y[negIdx]
##        y[negIdx, i] = -y[negIdx, i]
#        i+=1
    
#    i=0
#    while i < N-1:
##        zeroIdx = np.where(x[:,i]==0) # zero idx for dB cannot get inf value
#        nonZeroIdx = np.where(x[:,i]!=0)
#        # want to only calc dB of nonzero values
#        xdB[nonZeroIdx, i] = 20*np.log10(np.abs(x[nonZeroIdx, i]))
#        envdB = 20*np.log10(np.abs(env[:,i]))
#        # Using np.where to set each channel, instead of if statements
#        # constant gain region of graph
#        cstGainIdx = np.where(2*(envdB-T)< -KW)
#        # Compression region of graph
#        compressionIdx = np.where((2*np.abs(envdB-T))<=KW)        
#        # limiting region of graph
#        limitingIdx = np.where((2*(envdB-T)) > KW)
#        # Setting ydB indicies to be the approprite gain      
##        ydB[:,cstGainIdx] = xdB[cstGainIdx]
#        ydB[cstGainIdx, i] = xdB[cstGainIdx, i]        
##        ydB[:,compressionIdx] = xdB[compressionIdx]+(1/CR-1)*((
##                xdB[compressionIdx]-T+(KW/2))**2)/(2*KW)
#        ydB[compressionIdx, i] = xdB[compressionIdx, i]+(1/CR-1)*((
#                xdB[compressionIdx, i]-T+(KW/2))**2)/(2*KW)
##        ydB[:,limitingIdx] = T+((xdB[limitingIdx]-T)/CR)
#        ydB[limitingIdx, i] = T+((xdB[limitingIdx, i]-T)/CR)
#        # Adding markup gain
#        ydB[:, i] = ydB[:, i] + MG
#        # converting to linear
##        y = 10**(ydB/20)
##        y[:, i] = 10**(ydB[:, i]/20)
###        y=0
##        y[zeroIdx, i]=0
##        # getting negative idicies to convert back to negative
##        negIdx = np.where(x<0)
###        negIdx = np.where(x[:,i]<0)
##        # Setting to be negative
##        y[negIdx] = -y[negIdx]
###        y[negIdx, i] = -y[negIdx, i]
##        i+=1
#        
#        i+=1   
    '''Alternative use of y outside while loop'''
    y = 10**(ydB/20)
#        y=0
        
    zeroIdx = np.where(x==0)
    y[zeroIdx]=0
        # getting negative idicies to convert back to negative
    negIdx = np.where(x<0)
#        negIdx = np.where(x[:,i]<0)
        # Setting to be negative
    y[negIdx] = -y[negIdx]
#        y[negIdx, i] = -y[negIdx, i]
    """
    Old way using if statements for each condition for each channel
    # Left channel
    if 2*(envdB[0]-T)< -KW:
        ydB[0] = xdB[0]
    elif ((2*np.abs(envdB[0]-T))<=KW):
        ydB[0]=xdB[0]+(1/CR-1)*((xdB[0]-T+(KW/2))**2)/(2*KW)
    elif ((2*(envdB[0]-T)) > KW ):
        ydB[0] = T+((xdB[0]-T)/CR)            

    # Right channel
    if 2*(envdB[1]-T)< -KW:
        ydB[1] = xdB[1]
    elif ((2*np.abs(envdB[1]-T))<=KW):
        ydB[1]=xdB[1]+(1/CR-1)*((xdB[1]-T+(KW/2))**2)/(2*KW)
    elif ((2*(envdB[1]-T)) > KW ):
        ydB[1] = T+((xdB[1]-T)/CR)
    ### Would then add markup gain and convert to linear as normal here
    # Then would set to be negative here
    # Left channel
    if ( x[0,i] < 0 ):
        y[0,i] = -y[0,i]
    # Right channel
    if ( x[1,i] < 0 ):
        y[1,i] = -y[1,i]
    """
    """
    # Plot attempts
    py.figure()
    py.plot(x, legend='uncompressed')
    py.plot(y, legend='compressed')
    py.legend(loc=0)
    py.title('compressed')
    py.show()
    """
    return(y)
