#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 12:27:44 2018

@author: pi
"""

import visr
import gain_python, compression_function, envelope_function, filtFreq_function
import time
import numpy as np

class Pearable2(visr.CompositeComponent):
    """ Adder for the gain function """
    def __init__(self, context, name, parent, width, G, env, T, CR,
                 KW, MG, order, bands):
        super(Pearable2, self).__init__(context, name, parent)
        
        self.sum = PythonAdder(context, "Adder", self, len(bands), width)
        self.input = visr.AudioInputFloat("in", self, width)

        self.octBands = filtFreq(context, "bands", self, width, bands, order)
        self.output = visr.AudioOutputFloat("out", self, width)
        self.env = envelope(context, "e", self, width, bands)
        self.gain = gain_function(context, "g", self, width, G)
            
        self.comp = compression(context, "c", self, width, T,CR,KW,MG)
        
        self.audioConnection(self.input, self.octBands.audioPort("in"))
        for i in range(len(bands)):
#        self.gain = gain_function(context, "g%d" % i, self, width, G[i])
#            
#        self.comp = compression(context, "c%d" % i, self, width, T[i],CR[i],KW[i],MG[i])



        #        self.sum = octSum(context, "Adder", self, width)
        #        self.oct250 = filtFreq(context, "250", self, width, 250, order)
        #        self.oct500 = filtFreq(context, "500", self, width, 500, order)
        #        self.oct1k = filtFreq(context, "1000", self, width, 1000, order)
        #        self.oct2k = filtFreq(context, "2000", self, width, 2000, order)
        #        self.oct4k = filtFreq(context, "4000", self, width, 4000, order)
            
#            self.allowedTime = float(self.period())/self.samplingFrequency()
        #        self.freqSum = PythonAdder(context, "Adder", self, 2, width )
            
            
        #        
        ##        
        #        self.audioConnection(self.input, self.oct250.audioPort("in"))
        ##        self.audioConnection(self.oct250.audioPort("out"), self.output)
        #        
        #        self.audioConnection(self.input, self.oct2k.audioPort("in"))
        #        self.audioConnection(self.oct250.audioPort("out"), self.freqSum.audioPort("in0"))
        #        
        #        self.audioConnection(self.oct2k.audioPort("out"), self.freqSum.audioPort("in1"))
        #        
        #        self.audioConnection(self.freqSum.audioPort("out"), self.output) 
        #        



       
            self.audioConnection(self.octBands.audioPort("out%d" % i), self.env.audioPort("in%d" % i))
            self.audioConnection(self.octBands.audioPort("out%d" % i), self.comp.audioPort("in%d" % i))
            self.audioConnection(self.env.audioPort("out%d" % i), self.comp.audioPort("env%d" % i))
            self.audioConnection(self.comp.audioPort("out%d" % i), self.gain.audioPort("in%d" % i))
            self.audioConnection(self.gain.audioPort("out%d" % i), self.sum.audioPort("in%d" % i))
        
        
        self.audioConnection(self.sum.audioPort("out"), self.output)
        
    #        
#        t_start = time.time()
#        
##        self.audioConnection(self.input, self.oct250.audioPort("in"))
#        print(self.input.width, self.octBands.audioPort("in").width)
#        
#        
#        envTime = time.time()-t_start
#        print("Envelope takes %f seconds of %f" % (envTime, self.allowedTime))
#        t_start1 = time.time()
##        self.audioConnection(self.comp.audioPort("out"), self.output)
#        compTime = time.time()-t_start1
#        print("Compression takes %f seconds of %f" % (compTime,
#                                                      self.allowedTime))
#        
#       
#        t_start1 = time.time()
#        
#        
#        
#        
#        gainTime = time.time()-t_start1
#        print("Compression takes %f seconds of %f" % (gainTime,
#                                                      self.allowedTime))
#        print("VISR takes %f seconds of %f; sum: %f" % (time.time()-t_start,
#                                                        self.allowedTime,
#                                                        compTime+envTime))




            
    #        self.audioConnection(self.input, self.env.audioPort("in"))
    #        self.audioConnection(self.env.audioPort("out"), self.gain.audioPort("in"))
    ##        self.audioConnection(self.gain.audioPort("out"), self.output)
    #        self.audioConnection(self.input, self.gain.audioPort("in"))
    #        self.audioConnection(self.gain.audioPort("out"), self.output)
    
    #        self.audioConnection(self.input [0], self.env.audioPort("in"), [0])
    #        self.audioConnection(self.env.audioPort("out"), [0], self.comp.audioPort("in"), [0])
    #        self.audioConnection(self.comp.audioPort("out"), [0], self.gain.audioPort("in"), [0])
    #        self.audioConnection(self.gain.audioPort("out"), [0], self.output, [0])
    #        
    #        self.audioConnection(self.input [1], self.env.audioPort("in"), [1])
    #        self.audioConnection(self.env.audioPort("out"), [1], self.comp.audioPort("in"), [1])
    #        self.audioConnection(self.comp.audioPort("out"), [1elf.env.audioPort("o], self.gain.audioPort("in"), [1])
    #        self.audioConnection(self.gain.audioPort("out"), [1], self.output, [1])
        
class filtFreq(visr.AtomicComponent):
    def __init__( self, context, name, parent, width, bands, order):
        super(filtFreq,self).__init__( context, name, parent )
        self.input = visr.AudioInputFloat( "in", self, width )
        self.outputs = []
        self.bands = bands
        numOutputs = len(bands)
        self.order = order
        for outputIdx in range( 0, numOutputs):
            portName =  "out%d" % outputIdx
            outPort = visr.AudioOutputFloat( portName, self, width )
            self.outputs.append( outPort )
    def process( self ):
        x = self.input.data()
                # 3D filtered data
        filt = filtFreq_function.calculate(x, self.samplingFrequency(), self.bands, self.order)
        if len( self.outputs ) == 0:
            self.outputs[0].set( np.zeros( (self.input.width, self.period ), dtype = np.float32 ) )
        else:
            #acc = np.array(self.outputs[0])
            for idx in range(len(self.outputs)):
                self.outputs[idx].set(filt[:,:,idx])
#    def __init__(self, context, name, parent, numInputs, width, band, order):
#        super(filtFreq, self).__init__(context, name, parent)
#        self.input = visr.AudioInputFloat("in", self, width)
#        self.output = visr.AudioOutputFloat("out", self, width)
#        self.numInputs = numInputs
#        self.band = band
#        self.order = order
#    def process(self):
#        x = self.input.data()
#        y = filtFreq_function.calculate(x, self.samplingFrequency(), self.band, self.order)
#        print(y.shape)
#        self.output.set(y)
#        

class gain_function(visr.AtomicComponent):
    """ Adder for the gain function """
    def __init__(self, context, name, parent, width, G):
        super(gain_function, self).__init__(context, name, parent)  
        self.inputs = []
        self.outputs = []
        numInOut = len(G)
        for i in range(0, numInOut):
            portIn = "in%d" % i
            portOut = "out%d" % i
            inPorts = visr.AudioInputFloat(portIn, self, width)
            outPorts = visr.AudioOutputFloat(portOut, self, width)
            self.outputs.append(outPorts)
            self.inputs.append(inPorts)
        self.G = G

    def process(self):
        for idx in range(len(self.inputs)):
            x = self.inputs[idx].data()
#           y = self.output.data()
            y = gain_python.calculate(x, self.G[idx])
            self.outputs[idx].set(y)
#        self.output.set(gain_python.calculate(x, self.G, self.period(), y))


class envelope(visr.AtomicComponent):
    def __init__(self, context, name, parent, width, bands):
        super(envelope, self).__init__(context, name, parent)
        self.inputs = []
        self.outputs = []
        numInOut = len(bands)
        for i in range(0, numInOut):
            portIn = "in%d" % i
            portOut = "out%d" % i
            inPorts = visr.AudioInputFloat(portIn, self, width)
            outPorts = visr.AudioOutputFloat(portOut, self, width)
            self.outputs.append(outPorts)
            self.inputs.append(inPorts)
    def process(self):     
        for idx in range(len(self.inputs)):
            x = self.inputs[idx].data()
#           y = self.output.data()
            y = envelope_function.calculate(x)
            self.outputs[idx].set(y)
#        self.output.set(gain_python.calculate(x, self.G, self.period(), y))
#class envelope(visr.AtomicComponent):
#    """ Adder for the gain function """
#    def __init__(self, context, name, parent, width, tauA, tauR, env, T, CR, KW, MG):
#        super(envelope, self).__init__(context, name, parent)
#        self.input = visr.AudioInputFloat("in", self, width)
#        self.output = visr.AudioOutputFloat("out", self, width)
#        self.tauA, self.tauR,self.env = tauA, tauR, env
#        self.width = width  # num channels
#
#    def process(self):
#        x = self.input.data()
#        y = envelope_function.calculate(x,self.samplingFrequency(),self.tauA,
#                                         self.tauR,self.period(),self.env,
#                                         self.width)
#        self.output.set(y)
##        self.output.set(gain_python.calculate(x, self.G, self.period(), y))

#class compression(visr.AtomicComponent):
#    """ Adder for the gain function """
#    def __init__(self, context, name, parente, width, env,T,CR,KW,MG):
#        super(compression, self).__init__(context, name, parent)
#        self.input = visr.AudioInputFloat("in", self, width)
#        self.output = visr.AudioOutputFloat("out", self, width)
#        self.env,self.T,self.CR,self.KW,self.MG = env,T,CR,KW,MG
#  # num channels
#
#    def process(self):
#        x = self.input.data()
#        y = self.output.data()
#        y = compression_function.calculate(x,self.period(),self.env,self.T,self.CR,self.KW,self.MG,y)
#        self.output.set(y)
##        self.output.set(gain_python.calculate(x, self.G, self.period(), y))


class compression(visr.AtomicComponent):
    """ Adder for the gain function """
    def __init__(self, context, name, parent, width,T,CR,KW,MG):
        super(compression, self).__init__(context, name, parent)   
        self.inputs = []
        self.outputs = []
        self.env = []
        numInOut = len(T)
        for i in range(0, numInOut):
            portIn = "in%d" % i
            portEnv = "env%d" % i
            portOut = "out%d" % i            
            inPorts = visr.AudioInputFloat(portIn, self, width)
            envPorts = visr.AudioInputFloat(portEnv, self, width)
            outPorts = visr.AudioOutputFloat(portOut, self, width)
            self.outputs.append(outPorts)
            self.env.append(envPorts)
            self.inputs.append(inPorts)                      
        self.T,self.CR,self.KW,self.MG = T,CR,KW,MG
        # num channels

    def process(self):
        for idx in range(len(self.inputs)):
            x = self.inputs[idx].data()
            e = self.env[idx].data()
#           y = self.output.data()
            y = compression_function.calculate(x,self.period(),e,self.T[idx],self.CR[idx],self.KW[idx],self.MG[idx])
            self.outputs[idx].set(y)


class octSum( visr.AtomicComponent ):
    """ General-purpose add block for an arbitrary number of inputs"""
    def __init__( self, context, name, parent, width ):
        super(octSum, self).__init__( context, name, parent )
        self.input = visr.AudioInputFloat("in", self, width)
        self.output = visr.AudioOutputFloat( "out", self, width )
        
    def process( self ):
        x = self.input.data()
        y = np.sum(x, axis=2)
        self.output.set(y)


class PythonAdder( visr.AtomicComponent ):
    """ General-purpose add block for an arbitrary number of inputs"""
    def __init__( self, context, name, parent, numInputs, width ):
        super(PythonAdder,self).__init__( context, name, parent )
        self.output = visr.AudioOutputFloat( "out", self, width )
        self.inputs = []
        for inputIdx in range( 0, numInputs ):
            portName =  "in%d" % inputIdx
            inPort = visr.AudioInputFloat( portName, self, width )
            self.inputs.append( inPort )
    def process( self ):
        if len( self.inputs ) == 0:
            self.output.set( np.zeros( (self.output.width, self.period ), dtype = np.float32 ) )
        elif len( self.inputs ) == 1:
            self.output.set( np.array(self.inputs[0]))
        else:
            acc = np.array(self.inputs[0])
            for idx in range(1,len(self.inputs)):
                acc += np.array(self.inputs[idx])
            self.output.set( acc )
