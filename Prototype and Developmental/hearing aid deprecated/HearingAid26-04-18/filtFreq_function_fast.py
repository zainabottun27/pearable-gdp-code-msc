#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  9 12:30:45 2018

@author: pi
"""

import numpy as np
from scipy import signal
import time

def calculate(x, b, a, width, bufferSize, bands):
    y = np.zeros([width, bufferSize, len(bands)])
    if len(bands) == 1:
        y = signal.lfilter(b, a, x)
    else:
        for i in range(len(bands)):
            y[:,:,i] = signal.lfilter(b[i, :], a[i, :], x)
    y=y.reshape([x.shape[0]*len(bands), x.shape[1]]) #
    return(y)

def getFilterCoeffs(fs, bands, order):
    # self.sos array of sos coefficients only needs to be defined once
    fD = 2**0.5
    fU, fL = bands*fD, bands/fD  # gets the lower and upper octave band freqs
    Wn = np.array([fL, fU])/(0.5*fs)
    b = np.zeros([len(bands), order*2+1])
    a = np.zeros([len(bands), order*2+1])
    if len(bands) == 1:
        b, a = signal.butter(order, Wn, btype='band', output='ba')
    else:
        for i in range((len(bands))):
            if i == 0:
                b[i,:], a[i,:] = signal.butter(order*2, Wn[:,i][1], btype='low', output='ba')
            elif i == (len(bands)):
                b[i, :], a[i, :] = signal.butter(order, Wn[:,i][0], btype='high', output='ba')
            else:
                b[i,:], a[i,:] = signal.butter(order, Wn[:,i], btype='band', output='ba')
    return (b, a)