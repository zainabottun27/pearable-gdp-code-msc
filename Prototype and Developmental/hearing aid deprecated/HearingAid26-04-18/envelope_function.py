import numpy as np
# scipy iir filtering function gives continuous filtering scipy.signal.sosfilt
from scipy.signal import hilbert
#from scipy import signal

def calculate(x):
#def calculate(x,fs,tauA,tauR,N,env,numCh,env0=np.zeros(2)):
#    alphaA = np.exp(-1/(tauA*fs))
#    alphaR = np.exp(-1/(tauR*fs))

#    b, a = signal.ellip(13, 0.009, 80, 0.05, output='ba')
    analytic_signal = hilbert(x)
    envelope = np.abs(analytic_signal)
#    sos = signal.ellip(13, 0.009, 80, 0.05, output='sos')
#    ir = np.zeros(N)
#    ir[0] = 1.
#    y_tf = signal.lfilter(b, a, ir)
#    y_sos_tf = signal.sosfilt(sos, ir)
#    y_lfilt = signal.lfilter(b, a, x)
#    y_sos = signal.sosfilt(sos, x)
#    py.plot(y_tf, 'r', label='TF')
#    py.plot(y_sos_tf, 'k', label='SOS')
#    py.legend(loc='best')
#    py.show()
#    py.figure()
#    py.plot(y_lfilt, 'r', label='lfilt')
#    py.plot(y_sos, 'k', label='sos')
#    py.legend(loc='best')
#    py.show()
#    print(b)
    """
    i = 0
    # initilising alpha array
    alpha = np.zeros(numCh)
    while (i <= N-1):
        x_abs = np.abs(x[:,i])
        if (i == 0):
            y0 = env0
        else:
            y0 = env[:,i-1]
        # Using np.where instead of if to set both channels
        aIdx = np.where(x_abs > y0)
        rIdx = np.where(x_abs <= y0)
        """"""
        Old method using if statement for each channel
        if ( x_abs[0] > y0[0] ):
            alpha[0] = alphaA
        else:
            alpha[0] = alphaR
        # right channel
        if ( x_abs[1] > y0[1] ):
            alpha[1] = alphaA
        else:
            alpha[1] = alphaR
        """"""
        alpha[aIdx] = alphaA
        alpha[rIdx] = alphaR
        env[:,i] = alpha * y0 + (1 - alpha) * x_abs;
        env0 = env[:,N-1];
        i+=1
    """"""
    # plot attempts
    py.figure()
    py.plot(x, label='signal')
    py.plot(env, lebel='envelope')
    py.legend(loc=0)
    """"""
    """
    return(envelope)
