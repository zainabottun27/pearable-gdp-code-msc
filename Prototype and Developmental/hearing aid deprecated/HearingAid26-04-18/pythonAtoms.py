# -*- coding: utf-8 -*-
"""
Created on Sun Feb 26 16:41:01 2017

@author: Andreas Franck a.franck@soton.ac.uk
"""

# Make sure that the VISR python modules are on the Python search path (e.g., PYTHONPATH)

import visr
import numpy as np
import gain
import gain_python

class gain_function( visr.AtomicComponent ):
    """ Simple adder for three inputs """
    def __init__( self, context, name, parent, width, G ):
        super(gain_function,self).__init__( context, name, parent )
        self.input = visr.AudioInputFloat( "in", self, width )
##        self.input1 = visr.AudioInputFloat( "in1", self, width )
##        self.input2 = visr.AudioInputFloat( "in2", self, width )
        self.output = visr.AudioOutputFloat( "out", self, width )
        self.G = G
    def process( self ):
##        self.status( visr.StatusMessage.Information, "Called PythonAdder3::process()" )
##        self.output.set( self.input0.data() + self.input1.data() + self.input2.data() )
        x = self.input.data()
        y = self.output.data()
#        for i in range(self.input.width):
#            gain_python.calculate(x[i, :], self.G, self.period(), y[i, :])
        gain_python.calculate(x, self.G, self.period(), y)
        self.output.set( self.input.data() )


class pearable( visr.AtomicComponent ):
    """ Simple adder for three inputs """
    def __init__( self, context, name, parent, width, G ):
        super(pearable,self).__init__( context, name, parent )
        self.input = visr.AudioInputFloat( "in", self, width )
##        self.input1 = visr.AudioInputFloat( "in1", self, width )
##        self.input2 = visr.AudioInputFloat( "in2", self, width )
        self.output = visr.AudioOutputFloat( "out", self, width )
        self.G = G
    def process( self ):
##        self.status( visr.StatusMessage.Information, "Called PythonAdder3::process()" )
##        self.output.set( self.input0.data() + self.input1.data() + self.input2.data() )
        x = self.input.data()
        y = self.output.data()
        for i in range(self.input.width):
            gain.calculate(x[i, :], float(self.G), self.period(), y[i, :])


class PythonAdder1( visr.AtomicComponent ):
    """ Simple adder for three inputs """
    def __init__( self, context, name, parent, width ):
        super(PythonAdder1,self).__init__( context, name, parent )
        self.input = visr.AudioInputFloat( "in", self, width )
##        self.input1 = visr.AudioInputFloat( "in1", self, width )
##        self.input2 = visr.AudioInputFloat( "in2", self, width )
        self.output = visr.AudioOutputFloat( "out", self, width )
    def process( self ):
##        self.status( visr.StatusMessage.Information, "Called PythonAdder3::process()" )
        self.output.set( self.input.data() )


class PythonAdderG( visr.AtomicComponent ):
    """  """
    def __init__( self, context, name, parent, width, G_dB ):
        super(PythonAdderG,self).__init__( context, name, parent )
        self.input = visr.AudioInputFloat( "in", self, width )
##        self.input1 = visr.AudioInputFloat( "in1", self, width )
##        self.input2 = visr.AudioInputFloat( "in2", self, width )
        self.output = visr.AudioOutputFloat( "out", self, width )
        self.G_dB = G_dB
    def process( self ):
##        self.status( visr.StatusMessage.Information, "Called PythonAdder3::process()" )
        G = 10**(self.G_dB/20.0)
        self.output.set( self.input.data()*G )

class PythonAdder( visr.AtomicComponent ):
    """ General-purpose add block for an arbitrary number of inputs"""
    def __init__( self, context, name, parent, numInputs, width ):
        super(PythonAdder,self).__init__( context, name, parent )
        self.output = visr.AudioOutputFloat( "out", self, width )
        self.inputs = []
        for inputIdx in range( 0, numInputs ):
            portName =  "in%d" % inputIdx
            inPort = visr.AudioInputFloat( portName, self, width )
            self.inputs.append( inPort )
    def process( self ):
        if len( self.inputs ) == 0:
            self.output.set( np.zeros( (self.output.width, self.period ), dtype = np.float32 ) )
        elif len( self.inputs ) == 1:
            self.output.set( np.array(self.inputs[0]))
        else:
            acc = np.array(self.inputs[0])
            for idx in range(1,len(self.inputs)):
                acc += np.array(self.inputs[idx])
            self.output.set( acc )
