#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 16:41:01 2018

@author: pi
"""
import numpy as np
import HearingAidVariables as HA

bandIndex = np.array([0, 1])
bands, G, T, CR, KW, MG = HA.numBands(bandIndex)
x = np.ones([4, 20])
x[0,10], x[2,3], x[3, 19], x[1,1] = 15, 15, 15, 15
env = np.ones_like(x)

ydB=np.zeros_like(x)
xdB = np.zeros_like(x)
#T, CR, KW, MG = T[:,None], CR[:,None], KW[:,None], MG[:,None]

nonZeroIdx = np.where(x!=0)

xdB[nonZeroIdx] = 20*np.log10(np.abs(x[nonZeroIdx]))

envdB = 20*np.log10(np.abs(env))

cstGainIdx = np.where(2*(envdB-T[:,None])< -KW[:,None])

compressionIdx = np.where((2*np.abs(envdB-T[:,None]))<=KW[:,None])


limitingIdx = np.where((2*(envdB-T[:,None])) > KW[:,None])
ydB[cstGainIdx] = xdB[cstGainIdx]        

ydB[compressionIdx] = xdB[compressionIdx]+(1/CR[compressionIdx[0]]-1)*(
        (xdB[compressionIdx]-T[compressionIdx[0]]+(KW[compressionIdx[0]]/2)
        )**2)/(2*KW[compressionIdx[0]])

ydB[limitingIdx] = T[limitingIdx[0]]+((xdB[limitingIdx]-T[limitingIdx[0]])/CR[limitingIdx[0]])
ydB = ydB + MG[:,None]

y = 10**(ydB/20)
zeroIdx = np.where(x==0)
y[zeroIdx]=0
negIdx = np.where(x<0)
y[negIdx] = -y[negIdx]
