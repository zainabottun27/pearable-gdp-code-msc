#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 19 19:06:15 2018

@author: pi
"""


def calculate(x, G_dB):
    #print('running')
    G = 10**(G_dB[:,None]/20.0)
    y = G*x
#    for i in range(0, len(y)):
#        y[i] = x[i]*G
    return(y)
