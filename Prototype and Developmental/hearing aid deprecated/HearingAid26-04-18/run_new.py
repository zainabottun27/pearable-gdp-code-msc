
""" FUTURE IMPORTS """
from __future__ import print_function, division
import sys, os

def hearing_aid(current_cfg='standard.ini', offline=False):

	#=================#
	#                 #
	#    VARIABLES    #
	#                 #
	#=================#

	import ConfigParser  # for storing configurations in ini_file formats
        import json  # for storing configurations like a dictionary
        from numpy import array
        from time import sleep  # sleep allows a pause of a given no. of seconds in the code

	cfg = ConfigParser.ConfigParser()
	cfg.read(current_cfg) # Load current configuration file

	VARS = {} # Dictionary to hold variables
	FUNC = {} # Dictionary to hold function references
	INSTANCES = {} # Dictionary to hold instance references

	def get_defaults(): # Function to grab values from configuration file
		VARS['current_cfg'] = current_cfg
		VARS['f0'] = array(json.loads(cfg.get('filtering','f0')))
		VARS['ntaps'] = int(cfg.getfloat('filtering','ntaps'))
		VARS['bands'] = int(len(VARS['f0']))
		VARS['filtering_method'] = cfg.get('filtering','method')
		VARS['input_gain'] = cfg.getfloat('input_gain','value')
		VARS['feedback_control_threshold'] = cfg.getfloat('feedback_control','threshold')
		VARS['feedback_control_bandwidth'] = cfg.getfloat('feedback_control','bandwidth')
		VARS['data_collection_length'] = cfg.getfloat('data_collection','length')
		for i in xrange(0,VARS['bands']):
			VARS['band'+str(i+1)] = {'tauA_NR': cfg.getfloat('noise_reduction','attack_time_'+str(i+1)),
                                                 'tauR_NR': cfg.getfloat('noise_reduction','release_time_'+str(i+1)),
						 'tauA_comp': cfg.getfloat('compression','attack_time_'+str(i+1)),
						 'tauR_comp': cfg.getfloat('compression','release_time_'+str(i+1)),
						 'T': cfg.getfloat('compression','threshold_'+str(i+1)),
						 'CR':cfg.getfloat('compression','compression_ratio_'+str(i+1)),
						 'MG':cfg.getfloat('compression','makeup_gain_'+str(i+1)),
						 'KW':cfg.getfloat('compression','knee_width_'+str(i+1)),
						 'gain':cfg.getfloat('filtering','gain_'+str(i+1))}
	# print(VARS['band1'])
	FUNC['get_defaults'] = get_defaults
	get_defaults()

	#================#
	#                #
	#    SWITCHES    #
	#                #
	#================#

	SWITCHES = {}

	SWITCHES['gui'] = cfg.getboolean('startup','gui')
	SWITCHES['debug'] = cfg.getboolean('startup','debug')
	SWITCHES['collect_data'] = cfg.getboolean('startup','collect_data')

##	l = None
##
##	if SWITCHES['gui'] == False:
##		l = True
##	elif SWITCHES['gui'] == True:
##		l = True

	SWITCHES['audio'] = cfg.getboolean('startup', 'audio')
	SWITCHES['input_gain'] = cfg.getboolean('startup', 'audio')
	SWITCHES['filtering'] = cfg.getboolean('startup', 'audio')
	SWITCHES['feedback_control'] = cfg.getboolean('startup', 'audio')

	for i in xrange(0,VARS['bands']):
##		SWITCHES['band'+str(i+1)] = {'NR': l, 'comp': l, 'solo': False}
                SWITCHES['band'+str(i+1)] = {'NR': cfg.getboolean('noise_reduction', 'status_'+str(i+1)),
                                             'comp': cfg.getboolean('compression', 'status_'+str(i+1)),
                                             'solo': False}
	SWITCHES['quit'] = False


	#===============================#
	#                               #
	#    MISCELLANEOUS FUNCTIONS    #
	#                               #
	#===============================#

	def print_debug(x,*args):
		if SWITCHES['debug'] == True:
			print('# ',x)
			for arg in args:
				print('# ',arg)

	#======================#
	#                      #
	#    IMPORT MODULES    #
	#                      #
	#======================#

	print_debug('IMPORTING MODULES:','')

	print_debug('platform')
	import platform
	
	print_debug('time')
	from time import gmtime, strftime, time, sleep
	
	print_debug('os')
	import os
	
	print_debug('threading')
	import threading
	
	print_debug('numpy')
	import numpy as np
	from numpy import array, zeros, fromstring, int16, int32, float32, float64, log10, round
	
	print_debug('scipy.signal.firwin')
	from scipy.signal import firwin

	print_debug('','IMPORTING SIGNAL PROCESSING BLOCKS:','')

	print_debug('gain')
	import gain
	
	print_debug('filtering')
	import filtering
	
	print_debug('envelope_extraction')
	import envelope_extraction
	
	print_debug('compression')
	import compression
	
	print_debug('noise_reduction')
	import NR

	#=================================#
	#                                 #
	#    INPUT & OUTPUT PARAMETERS    #
	#                                 #
	#=================================#

	print_debug('Setting audio parameters...')

	IO = {}

	IO = {
		'period_size': cfg.getint('i/o','period_size'),
		'data_type': cfg.get('i/o','data_type'),
		'channels': cfg.getint('i/o','channels'),
		'card': cfg.get('i/o','card'),
		'fs': cfg.getint('i/o','fs')
		}
     
	FORMATS = {}

	from alsaaudio import PCM_FORMAT_S16_LE, PCM_FORMAT_S32_LE, PCM_FORMAT_FLOAT_LE, PCM_FORMAT_FLOAT64_LE

	FORMATS = {
			'int16':	{'bytes': 2, 'alsa': PCM_FORMAT_S16_LE,		'numpy': int16},
			'int32':	{'bytes': 4, 'alsa': PCM_FORMAT_S32_LE,		'numpy': int32},
			'float32':	{'bytes': 4, 'alsa': PCM_FORMAT_FLOAT_LE,		'numpy': float32},
			'float64':	{'bytes': 8, 'alsa': PCM_FORMAT_FLOAT64_LE,	'numpy': float64}
			}

	frame_size = FORMATS[IO['data_type']]['bytes']*IO['channels'] # Frame size (in bytes) for IO['data_type'] x number of IO['channels']

	#===========================#
	#                           #
	#    FILTER COEFFICIENTS    #
	#                           #
	#===========================#

	# Calculate lower limits
	VARS['fl'] = round(VARS['f0']/(2**(0.5)))
	# Calculate upper limits
	VARS['fu'] = round(VARS['f0']*(2**(0.5)))

	# Dummy variables for filter coefficients and FFT of filter coefficients
	VARS['h'] = [None]*VARS['bands']
	VARS['hfft'] = [None]*VARS['bands']

	# Calculate optimal FFT size
	VARS['filter_delay'] = VARS['ntaps']*2 # Delay from notch filter + actual filtering
	VARS['fft_size'] = int(2**np.ceil(np.log2(IO['period_size']+VARS['ntaps']*3-1)))

	print_debug('FFT size set to %s'%(VARS['fft_size']))

	# Generate filter coefficients for each band
	for i in xrange(0,VARS['bands']):
		VARS['h'][i] = firwin(VARS['ntaps'],[VARS['fl'][i]/(IO['fs']/2), VARS['fu'][i]/(IO['fs']/2)],pass_zero=False)
		print(VARS['h'][i].size)
		VARS['hfft'][i] = np.fft.rfft(VARS['h'][i],VARS['fft_size'])

	#=================#
	#                 #
	#    THREADING    #
	#                 #
	#=================#

	import audio_new as audio
        t_audio = audio.rt(IO,FORMATS,SWITCHES,VARS,FUNC,INSTANCES)
        

if __name__ == '__main__':
	hearing_aid()
