#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 11:15:16 2018

@author: pi
Script to test filtering
"""

from scipy.io import wavfile
from scipy import signal
import numpy as np
import HearingAidVariables as HA
import time

timeStart=time.time()

seconds = 1
order = 2
blockLength = 512
wavName = 'snarky'
fs, x = wavfile.read(wavName+'.wav')
bandIndex = np.array([0, 1, 2, 3, 4])
x = x.reshape([x.shape[1], x.shape[0]])  # swapping order of data
x = x[:, 0:blockLength]



bands, G, T, CR, KW, MG = HA.numBands(bandIndex)

filtData = np.zeros([x.shape[0], x.shape[1], len(bands)])
fD = 2**0.5
fU, fL = bands*fD, bands/fD  # gets the lower and upper octave band freqs
Wn = np.array([fL, fU])/(0.5*fs)
timeStart = time.time()

#
#sos = signal.butter(order, Wn, btype='band', output='sos')
#filtData = signal.sosfilt(sos, x, axis=1)
for i in range(len(bands)):
    sos = signal.butter(order, Wn[:,i], btype='band', output='sos')
    filtData[:,:,i] = signal.sosfilt(sos, x)

#for i in range(len(bands)):
#    b,a = signal.butter(order, Wn[:,i], btype='band', output='ba')
#    filtData[:,:,i] = signal.sosfilt(sos, x)
#filtData=filtData.reshape([x.shape[0]*len(bands), x.shape[1]]) # 10x512

print('Filter function takes %f seconds ' % (time.time()-timeStart))



processWidth = 10
b = np.zeros([processWidth, order*2+1])
a = np.zeros([processWidth, order*2+1])
for i in range((processWidth/2)):
    if i == 0:
        b[i,:], a[i,:] = signal.butter(order*2, Wn[:,i][1], btype='low', output='ba')
    elif i == (processWidth/2):
        b[i, :], a[i, :] = signal.butter(order, Wn[:,i][0], btype='high', output='ba')
    else:
        b[i,:], a[i,:] = signal.butter(order, Wn[:,i], btype='band', output='ba')


b[processWidth/2:] = b[:processWidth/2]
a[processWidth/2:] = a[:processWidth/2]
