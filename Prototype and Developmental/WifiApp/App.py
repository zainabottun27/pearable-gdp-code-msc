# Imports
import functools
import os
import random
import time
from piui import PiUi
from numpy import arange
import subprocess
# Importing hearing test function
from hearingTestWifi import runTest, pressed
# For reloading modules
import imp
# Import temp sensor stuff
import Digital_Temp_Sensor as tempSens
# Getting current dir
current_dir = os.path.dirname(os.path.abspath(__file__))


# Class for app
class DemoPiUi(object):
    # Inititation Function
    def __init__(self):
        self.title = None
        self.txt = None
        self.img = None
        self.ui = PiUi(img_dir=os.path.join(current_dir, 'imgs'))
        self.src = "sunset.png"

    # Overview page - Battery status and IP
    def page_overview(self):
        self.page = self.ui.new_ui_page(
                title="Overview", prev_text="Back", onprevclick=self.main_menu)
        self.page.add_textbox("IP: 192.168.4.1", "h1")
        self.page.add_element("hr")
        self.page.add_textbox("Battery Level 90%", "h1")

    # Page to connect headphones
    def page_connectHP(self):
        self.page = self.ui.new_ui_page(
                title="Connect Headphones", prev_text="Back",
                onprevclick=self.main_menu)
        self.title = self.page.add_textbox("Connect Headphones", "h2")
        connect = self.page.add_button("Connect!", self.connectHP)

    # Hearing Screening test page
    def page_hearing(self):
        self.page = self.ui.new_ui_page(
                title="Hearing Screening", prev_text="Back",
                onprevclick=self.main_menu)
        self.page.add_textbox("Hearing Screening", "h1")
        startButton = self.page.add_button("Start Test", self.onStartClick)
        self.page.add_element("br")  # Break between  buttons

    # Image page could be useful
    def page_images(self):
        self.page = self.ui.new_ui_page(
                title="Images", prev_text="Back", onprevclick=self.main_menu)
        self.img = self.page.add_image("sunset.png")
        self.page.add_element('br')
        button = self.page.add_button("Change The Picture", self.onpicclick)

    # Power options page
    def page_power(self):
        self.page = self.ui.new_ui_page(
                title="Power Management", prev_text="Back",
                onprevclick=self.main_menu)
        self.page.add_button("Reboot", self.restart)
        self.page.add_element('br')
        self.page.add_button("Power Off", self.shutdown)

    # Settings page for audio toggles
    def page_settings(self):
        self.page = self.ui.new_ui_page(
                title="Settings", prev_text="Back", onprevclick=self.main_menu)
        self.title = self.page.add_textbox("Audio Settings", "h1")
        self.list = self.page.add_list()
        self.list.add_item(
                "ANC", chevron=False, toggle=True,
                ontoggle=functools.partial(self.ontoggle, "ANC"))
        self.list.add_item(
                "Compression", chevron=False, toggle=True,
                ontoggle=functools.partial(self.ontoggle, "Compression"))
        self.list.add_item(
                "Layered Listening", chevron=False, toggle=True,
                ontoggle=functools.partial(self.ontoggle, "Layered Listening"))
        self.status = self.page.add_textbox(" ", "h1")
        self.page.add_element("hr")

    # Temperature page
    def page_temp(self):
        self.page = self.ui.new_ui_page(
                title="Temperature", prev_text="Back",
                onprevclick=self.page_sensors)
        tempBox = self.page.add_textbox("Internal Body Temperature", "h2")
        avg_temp = tempSens.avg_temp
        tempBox.set_text('Internal Body Temperature '+str(avg_temp) + " &deg;C")
        self.page.add_button("Refresh", self.refreshTemp)

    def refreshTemp(self):
        imp.reload(tempSens)  # Refresh/reload module
        avg_temp = tempSens.avg_temp
        tempBox.set_text('Internal Body Temperature '+str(avg_temp) + " &deg;C")
        

    # Heart rate page
    def page_heart(self):
        self.page = self.ui.new_ui_page(
                title="Heart Rate", prev_text="Back",
                onprevclick=self.page_sensors)
        rateBox = self.page.add_textbox("Heart Rate", "h2")
        for a in arange(60, 120, 5):
            rateBox.set_text('Heart Rate '+str(a) + " BPM")
            time.sleep(1)

    # Sensor overview page
    def page_sensors(self):
        self.page = self.ui.new_ui_page(
                title="Sensors", prev_text='Delete',
                onprevclick=self.main_menu)
        self.list = self.page.add_list()
        self.list.add_item("Temperature", chevron=True, onclick=self.page_temp)
        self.list.add_item("Heart-Rate", chevron=True, onclick=self.page_heart)

    # This page opens a console window
    def page_console(self):
        con = self.ui.console(
                title="Console", prev_text="Back", onprevclick=self.main_menu)
        con.print_line("Run console code")

    # Main menu setup page
    def main_menu(self):
        self.page = self.ui.new_ui_page(title="Hearable app")
        self.list = self.page.add_list()
        self.list.add_item("Overview", chevron=True,
                           onclick=self.page_overview)
        self.list.add_item("Connect Headphones", chevron=True,
                           onclick=self.page_connectHP)
        self.list.add_item("Hearing Screening",
                           chevron=True, onclick=self.page_hearing)
        self.list.add_item("Sensors", chevron=True, onclick=self.page_sensors)
        self.list.add_item("Settings", chevron=True,
                           onclick=self.page_settings)
        self.list.add_item("Power", chevron=True, onclick=self.page_power)
        self.list.add_item("Images", chevron=True, onclick=self.page_images)
        self.ui.done()

    # Function to shutdown Pi
    def shutdown(self):
        subprocess.Popen("sudo shutdown now", shell=True)

    # Function to reboot Pi
    def restart(self):
        subprocess.Popen("sudo reboot", shell=True)

    # Init main loop
    def main(self):
        self.main_menu()
        self.ui.done()

    # Connecting headphones
    def connectHP(self):
        self.title.set_text("Connecting Headphones ")
        subprocess.Popen("bash /usr/bin/hacktooth/connectHeadphones.sh",
                         shell=True)
        print("connecting headphones")

    # Unused function for input page
    def onhelloclick(self):
        print("onstartclick")
        self.title.set_text("Hello " + self.txt.get_text())
        print("Start")

    # Function to start hearing test - Adding button to click when hear sound
    def onStartClick(self):
        testButton = self.page.add_button("I can Hear it",
                                          self.onTestClick)
        # Starting hearing test
        testRes = runTest()[1]  # Getting test results to print to app
        htmlRes = testRes.replace('\n', '<br />')
        for i in htmlRes.split('<br />'):
            if 'Failed' in i:  # Adding Failed as heading
                self.page.add_textbox(i, 'h1')
            elif 'Hz' in i:  # Adding Freq bands as heading
                self.page.add_textbox(i, 'h2')
            elif 'passed' in i.lower():
                self.page.add_textbox(i, 'bi')
            else:
                self.page.add_textbox(i)

    def onTestClick(self):
        # Code to run enter press
        pressed(True)

    # Function to change picture in img folder
    def onpicclick(self):
        if self.src == "sunset.png":
            self.img.set_src("sunset2.png")
            self.src = "sunset2.png"
        elif self.src == "sunset2.png":
            self.src = "1.jpg"
            self.img.set_src("1.jpg")
        elif self.src == "1.jpg":
            self.src = "2.jpg"
            self.img.set_src("2.jpg")
        elif self.src == "2.jpg":
            self.src = "3.jpg"
            self.img.set_src("3.jpg")
        elif self.src == "3.jpg":
            self.src = "4.png"
            self.img.set_src("4.png")
        elif self.src == "5.jpg":
            self.src = "5.jpg"
            self.img.set_src("5.jpg")

    def ontoggle(self, what, value):
        if str(value) == 'False':
            self.status.set_text("Turning " + what + " off")
        # Run code here to do whatever the what is.
        if str(value) == 'True':
            self.status.set_text("Turning " + what + " on")


def main():
    piui = DemoPiUi()
    piui.main()


# Starting app
if __name__ == '__main__':
    main()
