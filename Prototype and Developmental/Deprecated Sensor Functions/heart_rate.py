# code adapted from heartBeats.py from on Git (Udayan Kumar)

import time
import Adafruit_MCP3008

# Software SPI configuration:
CLK  = 12
MISO = 23
MOSI = 24
CS   = 25
mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)

print('Reading MCP3008 values, press Ctrl-C to quit...')

def read_channel(channel):
    """Reads the data from ADC channels"""
    data = mcp.read_adc(channel)
    return data


def convert_volts(data, places):
    """This function converts data to voltage level rounded to specified number of decimal places"""
    volts = (data*3.3)/float(1023)
    volts = round(volts, places)
    return volts


# Convert data to heart rate
def convert_heart(Signal, places):
    """This function calculates heart rate to number of specified d.p"""
       
    lastTime = int(time.time()*1000)
    curState = 0
    thresh = 525  # mid point in the waveform
    P = 512
    T = 512
    stateChanged = 0
    sampleCounter = 0
    lastBeatTime = 0
    firstBeat = True
    secondBeat = False
    Pulse = False
    IBI = 600  # inter-beat interval
    rate = [0]*10
    amp = 100
    
    while True:
        #print("Analog reading = {}".format(Signal))
        curTime = int(time.time()*1000)

        sampleCounter += curTime - lastTime;      # keep track of the time in mS with this variable
        lastTime = curTime
        N = sampleCounter - lastBeatTime;     # monitor the time since the last beat to avoid noise
        #print (N, Signal, curTime, sampleCounter, lastBeatTime)

        #  find the peak and trough of the pulse wave
        if Signal < thresh and N > (IBI/5.0)*3.0 :  #       # avoid dichrotic noise by waiting 3/5 of last IBI
            if Signal < T :                        # T is the trough
              T = Signal                         # keep track of lowest point in pulse wave 

        if Signal > thresh and Signal > P:           # thresh condition helps avoid noise
            P = Signal                             # P is the peak
                                                # keep track of highest point in pulse wave
        # NOW IT'S TIME TO LOOK FOR THE HEART BEAT
        # signal surges up in value every time there is a pulse
        if N > 250 :                                   # avoid high frequency noise
            if  (Signal > thresh) and (Pulse == False) and (N > (IBI/5.0)*3.0):
              Pulse = True                               # set the Pulse flag when we think there is a pulse
              IBI = sampleCounter - lastBeatTime         # measure time between beats in mS
              lastBeatTime = sampleCounter               # keep track of time for next pulse

              if secondBeat :  # if this is the second beat, if secondBeat == TRUE
                secondBeat = False;                  # clear secondBeat flag
                for i in range(0,10):             # seed the running total to get a realisitic BPM at startup
                  rate[i] = IBI;                      

              if firstBeat :               # if it's the first time we found a beat, if firstBeat == TRUE
                firstBeat = False          # clear firstBeat flag
                secondBeat = True          # set the second beat flag
                continue                   # IBI value is unreliable so discard it

              # keep a running total of the last 10 IBI values
              runningTotal = 0;                  # clear the runningTotal variable

              for i in range(0,9):                # shift data in the rate array
                rate[i] = rate[i+1];                  # and drop the oldest IBI value
                runningTotal += rate[i];              # add up the 9 oldest IBI values

              rate[9] = IBI;                          # add the latest IBI to the rate array
              runningTotal += rate[9];                # add the latest IBI to runningTotal
              runningTotal /= 10;                     # average the last 10 IBI values
              BPM = 60000/runningTotal;               # how many beats can fit into a minute? that's BPM!
              print ("BPM: {}".format(BPM))

        if Signal < thresh and Pulse == True :   # when the values are going down, the beat is over
            Pulse = False                         # reset the Pulse flag so we can do it again
            amp = P - T                           # get amplitude of the pulse wave
            thresh = amp/2 + T                    # set thresh at 50% of the amplitude
            P = thresh                            # reset these for next time
            T = thresh

        if N > 2500 :                          # if 2.5 seconds go by without a beat
            thresh = 512                          # set thresh default
            P = 512                               # set P default
            T = 512                               # set T default
            lastBeatTime = sampleCounter          # bring the lastBeatTime up to date        
            firstBeat = True                      # set these to avoid noise
            secondBeat = False                    # when we get the heartbeat back
            print ("no beats found")

        time.sleep(0.005)  # delay has to be quite fast because normal heart rate for an adult is between 60 and 90 BPM
        # if the delay is 1 (second) it will not be able to detect a change in voltage
    
# Define sensor channels

heart_channel = 4

delay = 1

while True:
  
  # Read heart rate sensor data
  heart_level = read_channel(heart_channel)
  heart_volts = convert_volts(heart_level, 2)
  heart_rate = convert_heart(heart_level, 2)

  # Print our results
  #print(heart_level)
  print("Heart rate : {} ({}V) {} bpm".format(heart_level, heart_volts, heart_rate))


  # Wait before repeating loop
  #time.sleep(delay)
