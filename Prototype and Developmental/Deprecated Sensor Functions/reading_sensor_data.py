# Simple example of reading the MCP3008 analog input channels and printing
# them all out.
# Author: Tony DiCola
# License: Public Domain
import time

# Import SPI library (for hardware SPI) and MCP3008 library.
# import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008


# Software SPI configuration:
CLK  = 12
MISO = 23
MOSI = 24
CS   = 25
mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)

# Hardware SPI configuration:
# SPI_PORT   = 0
# SPI_DEVICE = 0
# mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))


#print('Reading MCP3008 values, press Ctrl-C to quit...')
## Print nice channel column headers.
#print('| {0:>4} | {1:>4} | {2:>4} | {3:>4} | {4:>4} | {5:>4} | {6:>4} | {7:>4} |'.format(*range(8)))
#print('-' * 57)
## Main program loop.
#while True:
#    # Read all the ADC channel values in a list.
#    values = [0]*8
#    for i in range(8):
#        # The read_adc function will get the value of the specified channel (0-7).
#        values[i] = mcp.read_adc(i)
#    # Print the ADC values.
#    print('| {0:>4} | {1:>4} | {2:>4} | {3:>4} | {4:>4} | {5:>4} | {6:>4} | {7:>4} |'.format(*values))
#    # Pause for half a second.
#    time.sleep(0.5)


def read_channel(channel):
  """Reads the data from ADC channels"""
  data = mcp.read_adc(channel)
  return data


def convert_volts(data, places):
  """This function converts data to voltage level rounded to specified number of decimal places"""
  volts = (data*3.3)/float(1023)
  volts = round(volts, places)
  return volts

# Convert data to temp
def convert_temp(data, places):
  """This function calculates temperature to number of specified d.p"""
  temp = ((data*330)/float(1023))-50  # -50 is min temp, 330 is temp range
  #temp = ((data*205)/float(1023))-55  # -50 is min temp, 330 is temp range
  temp = round(temp, places)
  return temp


# Convert data to heart rate
def convert_heart(data, places):
  """This function calculates heart rate to number of specified d.p"""
  heart = ((data*330)/float(1023))-50  # -50 is min temp, 330 is temp range
  #temp = ((data*205)/float(1023))-55  # -50 is min temp, 330 is temp range
  temp = round(temp, places)
  return temp

# Define sensor channels
temp_channel_elegoo = 0
temp_channel_tiny = 1
temp_channel_mus = 2
#heart_channel = 2

# Define delay between readings
#delay = 5

def temp(temp_channel, delay=5):

  while True:
    
    # Read temp sensor data
    temp_level = read_channel(temp_channel)
    temp_volts = convert_volts(temp_level, 2)
    temp = convert_temp(temp_level, 2)

    # Print our results
    print("Temp{} : {} ({}V) {} deg C".format(temp_channel, temp_level, temp_volts, temp))

    # Wait before repeating loop
    time.sleep(delay)


temp_elegoo = temp(temp_channel_elegoo)
temp_tiny = temp(temp_channel_tiny)
temp_mus = temp(temp_channel_mus)


  ## Read heart rate sensor data
  #heart_level = read_channel(heart_channel)
  #heart_volts = convert_volts(heart_level, 2)
  #heart_rate = convert_heart(heart_level, 2)

  ## Print our results
  #print("Heart rate : {} ({}V) {} bpm".format(heart_level, heart_volts, heart_rate))


  ## Wait before repeating loop
  #time.sleep(delay)
  
