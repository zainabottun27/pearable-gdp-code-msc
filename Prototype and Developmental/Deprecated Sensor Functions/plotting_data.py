import numpy as np
import matplotlib.pyplot as plt

datafile = open("Heart_Rate", 'r')
data = []

for lines in datafile:
    lines = lines.rstrip('\n')
    data.append(lines)
data = np.array(data)

datafile.close()

plt.figure()
plt.plot(data)
