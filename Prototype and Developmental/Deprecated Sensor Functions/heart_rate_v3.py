import time
import datetime
import numpy as np
import threading
import Adafruit_MCP3008
from MCP3008 import MCP3008


def read_data(channel):
    # Software SPI configuration:
    CLK  = 12
    MISO = 23
    MOSI = 24
    CS   = 25
    mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)
    data = mcp.read_adc(channel)
    return data


datafile = open("Heart_Rate", 'w')

n = 1000  # number of samples saved
counter = 0

while counter < n:

    datafile.write(str(read_data(4))+'\n')
    time.sleep(0.005)  # takes reading every 0.005 seconds
    counter += 1  # stops saving temp after n measurments

datafile.close()
