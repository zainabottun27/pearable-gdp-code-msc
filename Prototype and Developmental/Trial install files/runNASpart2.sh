#!/bin/bash

#Script to attempt setup BT ATDP with pulse audio mixer
# Change Name, Class and timeouts
sed --in-place -e 's/^Name = .*/Name = Hearable/' /etc/bluetooth/main.conf
sed --in-place -e 's/^DiscoverableTimeout.*/DiscoverableTimeout = 120/' /etc/bluetooth/main.conf
sed --in-place -e 's/^Class.*/Class = 0x240404/' /etc/bluetooth/main.conf

# Enable BT Audio Sinks/Media/Sources/Socket/Headset
sed --in-place -e 's/^\[General\]$/\[General\]\nEnable=Source,Sink,Media,Headset,Socket/' /etc/bluetooth/audio.conf

# update/regenerate bluetooth configuration (/var/lib/bluetooth/*)
service bluetooth restart

# Ensure name is correct, fixup Class, Discoverable on startup (choose whether or not)
# finally protect the config file otherwise Class gets rewritten on service restart
for btdevice in `find /var/lib/bluetooth/ -type f -name config -exec /bin/ls {} \; 2>/dev/null`
do
  chmod a=rw $btdevice
  sed --in-place -e 's/^name .*$/name Hearable/' $btdevice
  sed --in-place -e 's/^pairable .*$/pairable yes/' $btdevice
  sed --in-place -e 's/^class .*$/class  0x240404/' $btdevice
  #echo "onmode discoverable" >> $btdevice
  #echo "mode discoverable" >> $btdevice
  chmod a=r $btdevice
done

# reload config
service bluetooth restart

# repopulate scan
bt-adapter -d
## To make Blutooth Agent ALWAYS available, even on bluetoothd restarts:
#
cat <<EOF >/usr/local/sbin/btagent-auto
#!/bin/sh
BTPIN=0000
while true; do
  [ -f /etc/btagent-auto.conf ] && . /etc/btagent-auto.conf
  killall bluetooth-agent >/dev/null 2>&1
  killall -9 bluetooth-agent >/dev/null 2>&1
  bluetooth-agent \$BTPIN >/dev/null 2>&1
  sleep 3
done
EOF

chmod 0750 /usr/local/sbin/btagent-auto

cat <<EOF >/etc/btagent-auto.conf
# BLUETOOTH PINCODE FOR BTAGENT
BTPIN=1234
EOF

chmod 0600 /etc/btagent-auto.conf


# Now insert this in /etc/rc.local to spawn it at boot:
/usr/local/sbin/btagent-auto 0<&- 1>&- 2>&- &
