#!/bin/bash

#Run NAS Guide

#################################################################
# BLUETOOTH/DBUS/PULSE PERMISSIONS
#################################################################
sudo adduser root pulse-access
sudo adduser pi pulse-access

# Authorize PulseAudio - which will run as user pulse - to use BlueZ D-BUS interface:
############################################################################
cat <<EOF >/etc/dbus-1/system.d/pulseaudio-bluetooth.conf
<busconfig>

  <policy user="pulse">
    <allow send_destination="org.bluez"/>
  </policy>

</busconfig>
EOF
############################################################################

#################################################################
# CONFIGURE PULSEAUDIO
#################################################################


# Not strictly required, but you may need:
# In /etc/pulse/daemon.conf  change "resample-method" to either:
# trivial: lowest cpu, low quality
# src-sinc-fastest: more cpu, good resampling
# speex-fixed-N: N from 1 to 7, lower to higher CPU/quality
# resample-method = trivial

# Load  Bluetooth discover module in SYSTEM MODE:
############################################################################
cat <<EOF >> /etc/pulse/system.pa
#
### Bluetooth Support
.ifexists module-bluetooth-discover.so
load-module module-bluetooth-discover
.endif
EOF
############################################################################

# Create a systemd service for running pulseaudio in System Mode as user "pulse".
############################################################################
cat <<EOF >/etc/systemd/system/pulseaudio.service
[Unit]
Description=Pulse Audio

[Service]
Type=simple
ExecStart=/usr/bin/pulseaudio --system --disallow-exit --disable-shm --exit-idle-time=-1

[Install]
WantedBy=multi-user.target
EOF
############################################################################
systemctl daemon-reload
systemctl enable pulseaudio.service
# Restart Bluetooth and check its status (those errors are fine)
systemctl restart bluetooth
systemctl status bluetooth
echo "Now connect and pair through bluetoothctl"